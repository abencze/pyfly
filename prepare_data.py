# -*- coding: utf-8 -*-
'''
=============================================================================
* -------------------
* - Module history: -
* -------------------
*    * Created by A. Bencze in March 2014.
* -------------------
* - FUNCTIONS: -
* -------------------
*    + binary2int16
*    + set_on_off_time
*    + get_apd_hv
*    + get_rawsignal
*    + get_on_off_data (BA,2016 July)
*    + et_virtual_signals (RR)
* -------------------
=============================================================================
'''
import numpy as np
from xml.dom.minidom import parse
import xml.dom.minidom
import os
from scipy import signal as sg

#===============================================================================
def binary2int16(file):
    """
    ============ by A. Bencze, 2014. Modified by RR 2016 =======================
       converts binary data in standard python list of 16 bit integers.
       The binary data are *.dat files created by ABPD measurements in IDL.
       INPUT: file: string of the data fie with its path
                    (example:"../data/6539/data/Channel06.dat")
       OUTPUT: signal: list of integers (!!!No conversion to Volts)
    """
    import array
    import os
    signal=array.array('h')
    signal.fromfile(open(file,'rb'), os.path.getsize(file)/signal.itemsize)
    return signal

#===============================================================================
def set_on_off_time(data,t_vector,t_start,dt,start_on=True):
    """
    ============ by A. Bencze, 2014. =======================================

    INPUT: t_start: start time points for each slice.
                    This vector can be generated separately.
           dt     : duration of on_time and off_time, e.g. 0.02 sec (20ms)
           start_on: if True the series starts with on_time slice.
    OUTPUT:  data_slices:
             time:slices:
     """
    n_slices = np.size(t_start)
    i = 0
    for t_point in t_start:
        i_1 = np.array(np.where(t_vector>=t_point)).min()
        i_2 = np.array(np.where(t_vector<t_point+dt)).max()
        if i== 0:
            n_points    = np.size(t_vector[i_1:i_2])
            time_slices = np.zeros((n_slices,n_points),dtype=float)
            data_slices = np.zeros((n_slices,n_points),dtype=float)
            time_slices[i,:] = t_vector[i_1:i_2]
            data_slices[i,:] = data[i_1:i_2]
        else:
            time_slices[i,:] = t_vector[i_1:i_2]
            data_slices[i,:] = data[i_1:i_2]

        if start_on and i%2 == 0:
            print str(i)+". ON  time interval: " + str(t_point) + " - " + str(t_point+dt)
        elif start_on:
            print str(i)+". OFF time interval: " + str(t_point) + " - " + str(t_point+dt)
        elif start_on == False and i%2 == 0:
            print str(i)+". OFF time interval: " + str(t_point) + " - " + str(t_point+dt)
        else:
            print str(i)+". ON  time interval: " + str(t_point) + " - " + str(t_point+dt)
        i += 1
    return time_slices,data_slices
#===============================================================================
def get_apd_hv(shot,source='compass-beam',printinfo=False):
    '''
    Gets the high voltage setting for the APD camera from the config. xml file.
    '''

    if source == 'compass-beam':
        path = '/data/COMPASS/'+str(shot)+'/info/'+str(shot)+'_config_apd.xml'

    xml_doc       = xml.dom.minidom.parse(path)
    shot_settings = xml_doc.getElementsByTagName("ShotSettings")[0]
    adc_settings  = shot_settings.getElementsByTagName("ADCSettings")[0]
    apd_HV        = adc_settings.getElementsByTagName("High_Voltage")[0]
    apd_hv        = int(apd_HV.getAttribute("Value")) # volts
    if printinfo:
        print 'APD HV: '+str(apd_hv)+' [V]'
    return apd_hv
#===============================================================================
def get_trigger_time(shot,source='compass-beam',printinfo=False):
    '''
    Gets the trigger time setting for the APD camera from the config. xml file.
    '''

    if source == 'compass-beam':
        path = '/data/COMPASS/'+str(shot)+'/info/'+str(shot)+'_config_apd.xml'

    xml_doc       = xml.dom.minidom.parse(path)
    shot_settings = xml_doc.getElementsByTagName("ShotSettings")[0]
    timing        = shot_settings.getElementsByTagName("Timing")[0]
    trigger       = timing.getElementsByTagName("Trigger")[0]
    t0            = trigger.getAttribute("Value") # seconds
    if printinfo:
        print 'APD trigger: '+str(t0)+' [sec]'
    return t0
#===============================================================================
def get_digi_res(shot,source='compass-beam',printinfo=False):
    '''
    Gets the digital bit resolution for the APD-ADC  from the config. xml file.
    '''

    if source == 'compass-beam':
        path = '/data/COMPASS/'+str(shot)+'/info/'+str(shot)+'_config_apd.xml'

    xml_doc       = xml.dom.minidom.parse(path)
    shot_settings = xml_doc.getElementsByTagName("ShotSettings")[0]
    adc_settings  = shot_settings.getElementsByTagName("ADCSettings")[0]
    bit_res        = adc_settings.getElementsByTagName("resolution")[0]
    digi_res        = int(bit_res.getAttribute("Value")) # volts
    if printinfo:
        print 'digi. res.: '+str(digi_res)+' [bits]'
    return digi_res
#===============================================================================
def get_sample_freq(shot,source='compass-beam',printinfo=False):
    '''
    Gets the sampling frequency setting for the APD camera from the config. xml file.
    '''

    if source == 'compass-beam':
        path = '/data/COMPASS/'+str(shot)+'/info/'+str(shot)+'_config_apd.xml'

    xml_doc       = xml.dom.minidom.parse(path)
    shot_settings = xml_doc.getElementsByTagName("ShotSettings")[0]
    timing        = shot_settings.getElementsByTagName("Timing")[0]
    samplefreq    = timing.getElementsByTagName("SampleFreq")[0]
    fs            = samplefreq.getAttribute("Value") # MHz
    if printinfo:
        print 'sample freq: '+str(fs)+' [MHz]'
    return fs
#===============================================================================
def get_from_cdb(shot,channel=1):
    '''
    This function reads APD data from the CompassDataBase.
    '''
    #str_id = "APD/LI_BEAM:"+str(shot)+":1"
    str_id = "APD/LI_BEAM:"+str(shot)
    from pyCDB.client import CDBClient as cdb_client
    cdb = cdb_client()
    s   = cdb.get_signal(str_id)
    d   = s.data[:,channel-1]
    return d
#===============================================================================
def get_Ip(shot):
    '''
    This function reads the plasma current (Rogowski coil) as a function of time
    from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("I_plasma_Rogowski_coil_RAW/MAGNETICS_RAW:"+str(shot))
    return  signal.time_axis.data,-1* signal.data/1000 #Ip [kA], t [ms]
#===============================================================================
def get_ne(shot):
    '''
    This function reads the line averaged electron density as a function of time
    from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("MARTE_NODE.PositionDataCollection2.CorrectedDensity/MARTE:"+str(shot))
    return  signal.time_axis.data, signal.data #ne [1e19 m^-3], t [ms]
#===============================================================================
def get_H_alpha(shot):
    '''
    This function reads the plasma current (Rogowski coil) as a function of time
    from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("H_alpha/SPECTROMETRY_RAW:"+str(shot))
    return  signal.time_axis.data, -1*signal.data #Intensity [a.u.], t [ms]
#===============================================================================
def get_R_midplane_LCFS(shot):
    '''
    This function reads the separatrix position as a function of time
    from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("R_mid_out/EFIT:"+str(shot))# in [m]
    return signal.time_axis.data, signal.data # R [m],t [ms]
#===============================================================================
def get_RMP_current(shot):
    '''
    This function reads the "RMP_PS_1_Rogowski_coil_RAW"  as a function of time
    from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("RMP_PS_1_Rogowski_coil_RAW/MAGNETICS_RAW:"+str(shot))
    return signal.time_axis.data, signal.data/1000 # I [kA],t [ms]
#===============================================================================
def get_NBI_1_current(shot):
    '''
    This function reads the "RMP_PS_1_Rogowski_coil_RAW"  as a function of time
    from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("NBI1_Ibeam/NBI:"+str(shot))
    return signal.time_axis.data, signal.data # I [kA],t [ms]
#===============================================================================
def get_NBI_2_current(shot):
    '''
    This function reads the "RMP_PS_1_Rogowski_coil_RAW"  as a function of time
    from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("NBI2_Ibeam/NBI:"+str(shot))
    return signal.time_axis.data, signal.data # I [kA],t [ms]
#===============================================================================
def get_R_APD(shot):
    '''
    This function reads the APD channel position along the Li-beam from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("APD.R_axis/LI_BEAM:"+str(shot))
    return signal.data/100 #in [m]
#===============================================================================
def get_R_APD(shot):
    '''
    This function reads the APD channel position along the Li-beam from CDB.
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("APD.R_axis/LI_BEAM:"+str(shot))
    return signal.data/100 #in [m]
#===============================================================================
def get_rawsignal(shot, source='compass-beam', channel=12, offset=True, t0=0.8, t1=0.8, \
                  t2=1.8, fs=2e6, digi_res=14, printinfo=False, with_time=False):
    '''
    This is the first version! Has to be improved later!
    '''
    # fs = 2e6
    # t0 = 0.912
    if source == 'kstar-beam':
        # --------
        # this path is for the beam.rmki.kfki.hu data for KSTAR
        # --------
        path = "/data/KSTAR/APDCAM" + str(shot)
        file, t0, fs = get_config(shot, channel, source, path, t0, fs)
        # --------
        standard_list_from_file = binary2int16(file)
    elif source == 'abp-ltserv':
        # --------
        # this path is for the local ltserv abp data
        # --------
        # channels  = np.array(range(32))
        path = "/compass/home/bencze/data_abp/" + str(shot)
        file, t0, fs = get_config(shot, channel, source, path, t0, fs)
        # --------
        standard_list_from_file = binary2int16(file)
    elif source == 'abp-beam':
        # --------
        # this path is for the abp data on the 'beam.rmki.kfki.hu' server
        # --------
        path = "/data/COMPASS/" + str(shot)
        file, t0, fs = get_config(shot, channel, source, path, t0, fs)
        # --------
        standard_list_from_file = binary2int16(file)
    elif source == 'abp-local':
        # --------
        # this path is for the local abp data
        # --------
        path = "/data/" + str(shot)
        file, t0, fs = get_config(shot, channel, source, path, t0, fs)
        # --------
        standard_list_from_file = binary2int16(file)
    elif source == 'compass-ltserv':
        # --------
        # this path is for the local ltserv data
        # --------
        path = "/compass/home/bencze/data/" + str(shot)
        file, t0, fs = get_config(shot, channel, source, path, t0, fs)
        # --------
        standard_list_from_file = binary2int16(file)
    elif source == 'compass-cdb':
        # --------
        # this reads data from compass datatbase starting from ltserv
        # --------
        standard_list_from_file = get_from_cdb(shot, channel=channel)
    elif source == 'compass-beam':
        # --------
        # this path is for the beam.rmki.kfki.hu data
        # --------
        path = "/data/COMPASS/" + str(shot)
        file, t0, fs = get_config(shot, channel, source, path, t0, fs)
        # --------
        standard_list_from_file = binary2int16(file)
    elif source == 'compass-local':
        # --------
        # this path is for the local computer data
        # --------
        #path = "D:\\Wigner\\TDK\\shots\\" + str(shot)
        path = '/data/'+str(shot)
        file, t0, fs = get_config(shot, channel, source, path, t0, fs)
        # --------
        standard_list_from_file = binary2int16(file)
    else:
        # --------
        # this path is for optional folder
        # --------
        path = source + "/" + str(shot)
        file, t0, fs = get_config(shot, channel, source, path, t0, fs)
        # --------
        standard_list_from_file = binary2int16(file)

    data, time_full = get_data_and_time(digi_res, fs, standard_list_from_file, t0)

    if shot >= 13314:
        data *= -1
    # calculation of offset:
    baseline = np.mean(data[np.all([time_full > t0, time_full < t0 + 0.02], axis=0)])
    if offset == True:
        raw_signal = data - baseline
    else:
        raw_signal = data

    index = np.where(np.logical_and(time_full >= t1, time_full <= t2))
    time = time_full[index]
    if printinfo:
        print "====== get_rawsignal info ======"
        print "shot  : " + str(shot)
        print "channel  : " + str(channel)
        print "source: " + source
        print "time interval: " + " [" + str(t1) + "," + str(t2) + "]"
        print "data lengths: " + str(time.size)
        print "================================"
    if with_time:
        return raw_signal[index], time_full[index], index
    return raw_signal[index]

def get_data_and_time(digi_res, fs, standard_list, t0):
    data = np.array(standard_list)  # creates a numpy array from a standard python list
    data = data * (2.0 / 2 ** digi_res)  # calibrated in Volts
    time_full = np.array([t * 1 / fs for t in range(data.size)]) + t0
    return data, time_full

def get_config(shot, channel, source, path, t0, fs):
    if "abp" in source:
        channels = np.array([5, 3, 1, 12, 14, 7, 18, 19, 11, 13, 15, 10, 2, 4, 0, 16, 9, 6, 17, 8])
        path += "/data/ABP"
    else:
        xml_path = path + "/info/" + str(shot) + "_config_apd.xml"
        if os.path.exists(xml_path):
            config = xml.dom.minidom.parse(xml_path)

            timing = config.getElementsByTagName("Timing")[0]
            t0 = float(timing.getElementsByTagName("Trigger")[0].getAttribute("Value").replace(u'\N{MINUS SIGN}', '-'))
            sample_freq = timing.getElementsByTagName("SampleFreq")[0]
            sample_freq_unit = sample_freq.getAttribute("Unit")

            fs = float(sample_freq.getAttribute("Value").replace(u'\N{MINUS SIGN}', '-'))
            if sample_freq_unit == "Hz":
                fs *= 1
            elif sample_freq_unit == "kHz":
                fs *= 1e3
            elif sample_freq_unit == "MHz":
                fs *= 1e6
            elif sample_freq_unit == "GHz":
                fs *= 1e9
            else:
                raise ValueError("Wrong unit of SampleFreq in"
                                 + str(shot) +"_config_apd.xml file!")

            channels_map = config.getElementsByTagName("ShotSettings")[0].getElementsByTagName("Optics")[0]
            channels = np.array([])

            for i in range(1, 19):
                channels = np.append(channels,
                                     int(channels_map.getElementsByTagName("APD-" + str(i))[0].getAttribute("Value")))
        elif shot < 12425:
            channels = np.array([31, 30, 29, 28, 4, 5, 6, 7, 0, 21, 20, 11, 12, 13, 14, 15, 16, 17])
        else:
            channels = np.array([17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0])
        path += "/data"
    return path + "/Channel%02d.dat" % channels[channel - 1], t0, fs


#===============================================================================
def get_on_off_data(shot,source='compass-beam',channel=10,timefile=0,\
                    trange=[1.1,1.2],t1=0.912,t2=1.9,digi_res=14,fs=2e6,\
                    offset=True):
    '''
    This function returns a 2D data array with each line containing a time series
    coresponding to time slices given in a time file created by select_time function in
    signal_processing.py module.
    INPUT: same as get_rawsignal, except:
        timefile: the on or off time file with its full path, e.g.:
              timefile = '/compass/home/bencze/data/12230/on_off_times/on_times.txt'
        trange: time range in seconds to be analysed.
    OUTPUT:
        data_array: 2D array NxM, where N is the number of time ON/OFF intervals in trange
        and M is the number of time points within a given time interval.
    '''
    if timefile == 0:
        print 'You should specify a time file!'
        return []
    #print 'Imports modules'
    #print '...'
    #import prepare_data as pd
    #print 'Done'

    print 'Reads data from '+source
    print '...'
    data = get_rawsignal(shot,source=source,channel=channel,offset=offset,\
	                 t1=t1,t2=t2,digi_res=digi_res,fs=fs)
    time = np.array([t*(1/fs) for t in range(data.size)])+t1
    print 'Done'

    print 'Reads timefile: ' + timefile
    print '...'
    time_slices = np.loadtxt(timefile)
    time_starts = time_slices[:,0]
    time_stops  = time_slices[:,1]
    print 'Done'
    print 'Process time segments'
    print '...'
    index = np.where(np.logical_and(time_starts>=trange[0],\
                     time_starts<=trange[1]))
    time_starts_relevant = time_starts[index]
    time_stops_relevant  = time_stops[index]
    data_len = np.zeros(len(time_starts_relevant))
    for i in np.arange(0,len(time_starts_relevant)):
        data_index = np.where(np.logical_and(time>=time_starts_relevant[i], \
                              time<=time_stops_relevant[i]))
        data_i       = data[data_index]
        data_len[i]  = len(data_i)
    print 'Minimal time segment length:'+str(np.min(data_len))
    print 'Maximal time segment length:'+str(np.max(data_len))

    min_data_len = int(np.amin(data_len))
    data_array = np.zeros((len(time_starts_relevant),min_data_len))
    for i in np.arange(0,len(time_starts_relevant)):
        data_index = np.where(np.logical_and(time>=time_starts_relevant[i], \
                              time<=time_stops_relevant[i]))
        data_i       = data[data_index]
        time_i       = time[data_index]
        data_array[i,:] = data_i[0:min_data_len]
    print 'Done'
    return data_array,time_i[0:min_data_len]


#========================================================================================================

def get_virtual_signals(shot, source='compass-beam', channel=12, offset=True, t0=0.8, t1=0.8, \
                        t2=1.8, fs=2e6, digi_res=14, printinfo=False, with_time=False, divertion_time_in_microsec=10,
                        decim=False):
    real_signal, time, index = get_rawsignal(shot, source=source, channel=channel, offset=offset, t0=t0, t1=t1, \
                                             t2=t2, fs=fs, digi_res=digi_res, printinfo=printinfo, with_time=True)
    number_of_points = int(divertion_time_in_microsec*fs/1e6)
    virtual_signal_first = fill_virtual_signals(real_signal, True, number_of_points)
    virtual_signal_second = fill_virtual_signals(real_signal, False, number_of_points)

    if decim:
        real_signal = sg.decimate(real_signal, 50)
        virtual_signal_first = sg.decimate(virtual_signal_first, 50)
        virtual_signal_second = sg.decimate(virtual_signal_second, 50)
        dt = 1.0 / (real_signal.size - 1)
        time = np.array([t * dt for t in range(np.size(real_signal))]) + t1

    if with_time:
        return real_signal, virtual_signal_first, virtual_signal_second, time
    return real_signal, virtual_signal_first, virtual_signal_second


def fill_virtual_signals(signal, first, number_of_points):
    index = 0
    result = list(signal)
    if not first:
        index = number_of_points
    while (True):
        if index + 2 * number_of_points < len(signal):
            before_points = signal[index:index + number_of_points]
            after_ponts = signal[index + 2 * number_of_points:index + 3 * number_of_points]
            min = np.min([np.min(before_points), np.min(after_ponts)])
            max = np.max([np.max(before_points), np.max(after_ponts)])
            range = max - min
            changed_value = np.random.rand(number_of_points) * range + min
            if not first:
                result[:number_of_points] = changed_value
            else:
                result[len(result)-1-number_of_points:] = changed_value
            result[index + number_of_points:index + 2 * number_of_points] = changed_value
            index += 2 * number_of_points
        else:
            break

    return np.array(result)