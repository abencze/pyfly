# -*- coding: utf-8 -*-
'''
=============================================================================
* -------------------
* - Module history: -
* -------------------
*    * Created by A. Bencze (BA) in February 2014.
*    * Some new functions added by R. Reho (RR) from early 2016.
* -------------------
* - FUNCTIONS: -
* -------------------
*    + light_profile(BA)
*    + calibrated_light_profile(BA)
*    + cross_spectrum(BA)
*    + smooth(BA)
*    + ccf(BA)
*    + ccf_def(BA)
*    + iir_bandpass_filter(BA)
*    + fir_bandpass_filter(BA)
*    + iir_highpass_filter(BA)
*    + apsd(BA)
*    + aver_psd(BA)
*    + logspace_freq(BA)
*    + signal_splitter(BA)
*    + rescaled_pdf(BA)
*    + sekewness_profile(BA)
*    + rms(BA)
*    + rel_fluc_ampl(BA)
*    + rt_correlation(BA)
*    + std_profile (BA)
*    + spectral_exponent (BA)
*    + data_read(RR)
*    + remove_peak(RR)
*        + peakRemove
*        + peakDetection
*        + peakUI
*    + select_time(RR)
*        + select_time_UI
*        + select_time_write_to_file
*        + selecting_times
*    + deflection_period(BA)
*    + photon_cut (BA)
*    + removing_blob (RR)
*        + counting_blob
*        + removing_blobs
*        + multiply_blob_removing
* -------------------
'''
#=============================================================================
import numpy as np
import prepare_data as pd
from scipy import signal
from scipy.signal import butter, lfilter, detrend
from scipy.stats import skew
import signal_simulator as sim
import matplotlib.pyplot as plt
import time as times
import random as random
#=============================================================================

def light_profile(shot,source='compass-beam',trange=[1.1,1.11],\
                  channels=np.arange(1,19),t0=0.912,fs=2e6,\
                  plot=False,show_ch=11,tlim=[1.1597,1.1608],digi_res=12):
    '''
    Returns the light profile averaged over a time interval given by 'trange'.
    INPUT:
    --------
        shot:     shot number
        source:   data source see get_rawsignal from the prepare_data module.
        trange:   time range for profile calculation.
        channels: 1d numpy array of channel no. we use for profile calculation.
        t0:       trigger time in sec.
        fs:       sampling frequency in Hz.
        plot:     if True a plot is produced.
        show_ch:  if plot=True, denotes the ploted channel.
        tlim:     time range for the ploted signal.
    OUTPUT:
    --------
        prof:     1d array of profile data, the same length as channels.
    '''
    prof    = np.empty(len(channels))
    for i,channel in enumerate(channels):
        data = pd.get_rawsignal(shot,source=source,channel=channel,offset=True,\
               t1=trange[0],t2=trange[1],t0=t0,digi_res=digi_res)
	prof[i]  = np.mean(data)
    if plot:
        fig = plt.figure()
        ax1 = fig.add_subplot(211)
        ax1.plot(channels,prof,'--o',color='red',label='t=[%1.5f , %1.5f s]'%(trange[0],trange[1]))
        ax1.set_xlabel("channel")
        ax1.set_ylabel("Mean Value [V]")
        ax1.legend(loc='best')
        ax2 = fig.add_subplot(212)
        data = pd.get_rawsignal(shot,source=source,channel=show_ch,offset=True,t1=t0,t2=t0+1,digi_res=digi_res)
        time = np.array([t*(1/fs) for t in range(data.size)])+t0
        ax2.plot(time,data,label='#'+str(shot)+'/CH:'+str(show_ch))
        ax2.legend()
        ax2.axvline(x=trange[0],color='red',linewidth=2)
        ax2.axvline(x=trange[1],color='red',linewidth=2)
        ax2.set_xlim([tlim[0],tlim[1]])
        ax2.set_xlabel("time [s]")
        ax2.set_ylabel("Signal [V]")
        plt.show()
    return prof
#=============================================================================
def calibrated_light_profile(shot,source='compass-beam',trange=[1.1,1.2],\
                  channels=np.arange(1,19),t0=0.8,fs=2e6,cal_shot=13307,\
		  cal_trange=[1.42,1.45],plot=False,cal_plot=False):
    cal = light_profile(cal_shot,source=source,t0=t0,trange=cal_trange,\
			channels=channels,plot=plot)
    prof = light_profile(shot,source=source,t0=t0,trange=trange,\
			channels=channels,plot=plot)
    calib_prof = prof/cal
    if cal_plot:
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.plot(channels,calib_prof,'--o',color='red',label='t=[%1.5f , %1.5f s]'%(trange[0],trange[1]))
        ax1.set_xlabel("channel")
        ax1.set_ylabel("calibrated light profile [a.u.]")
        ax1.legend(loc='best')
	plt.show()
    return channels,calib_prof
#=============================================================================
def cross_spectrum(x,y,fs=2e6):
    '''
    Returns the cross power density of two equaly sampled signals.

    INPUT:
    --------
        x,y: the input signal (if x=y APSD)
        fs:  is the sampling rate (e.g. 2e6 Hz,default)
    OUTPUT:
    --------
        (cpsd,phase,freq): a tuple of cross power density absolute value, its
        phase and the frequency vector.
    EXAMPLE:
    --------
    '''
    x_fft = np.fft.fft(x)
    y_fft = np.fft.fft(y)
    cpsd_full = np.abs(x_fft*np.conjugate(y_fft))# positve and negative frequencies
    cpsd = cpsd_full[0:(np.size(x_fft)+1)/2]# positive frequencies only
    phase = np.angle(x_fft[0:(np.size(x_fft)+1)/2])-np.angle(y_fft[0:(np.size(x_fft)+1)/2])# phase in radians
    fn    = float(fs)/2
    N     = np.size(cpsd)
    df_0  = fn/N # 1/2 is NOT needed here, since cpsd is already the half of x_fft
    freq  = np.arange(N)*df_0
    return (cpsd,phase,freq)
#=============================================================================
def smooth(x, window_len=10, window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    INPUT:
    --------
        x:          the input signal
        window_len: the dimension of the smoothing window
        window:     the type of window from 'flat', 'hanning', 'hamming',
                    'bartlett', 'blackman'
                    flat window will produce a moving average smoothing.

    OUTPUT:
    --------
        the smoothed signal

    EXAMPLE:
    ---------
    import numpy as np
    t = np.linspace(-2,2,0.1)
    x = np.sin(t)+np.random.randn(len(t))*0.1
    y = smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    """

    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."

    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."

    if window_len < 3:
        return x

    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
    s=np.r_[2*x[0]-x[window_len:1:-1], x, 2*x[-1]-x[-1:-window_len:-1]]
    #print(len(s))

    if window == 'flat': #moving average
        w = np.ones(window_len,'d')
    else:
        w = getattr(np, window)(window_len)
    y = np.convolve(w/w.sum(), s, mode='same')
    return y[window_len-1:-window_len+1]

#=============================================================================
def ccf(x, y, axis=None, norm = True, taurange=[]):
    """Computes the cross-correlation function of two series `x` and `y`.
    Note that the computations are performed on deviations from
    average. The mean value is subtracted.
    Returns the values of the cross-correlation at different time lags.

    INPUT:
    ---------
    `x`    : 1D MaskedArray Time series.
    `y`    : 1D MaskedArrayTime series.
    `axis` : integer *[None]*Axis along which to compute (0 for rows, 1 for cols).
             If `None`, the array is flattened first.
    OUTPUT:
    --------
    'corr' : cross corelation values. The length is 2*size(x)
    'lag'  : lag values, from -size(x) to +size(x)
    """
#    assert(x.ndim == y.ndim, "Inconsistent shape !")
#    assert(x.shape == y.shape, "Inconsistent shape !")
    if axis is None:
        if x.ndim > 1:
            x = x.ravel()
            y = y.ravel()
        npad = x.size + y.size
        xanom = (x - x.mean(axis=None))
        yanom = (y - y.mean(axis=None))
        Fx = np.fft.fft(xanom, npad, )
        Fy = np.fft.fft(yanom, npad, )
        iFxy = np.fft.ifft(Fx.conj()*Fy).real
        varxy = np.sqrt(np.inner(xanom,xanom) * np.inner(yanom,yanom))
    else:
        npad = x.shape[axis] + y.shape[axis]
        if axis == 1:
            if x.shape[0] != y.shape[0]:
                raise ValueError, "Arrays should have the same length!"
            xanom = (x - x.mean(axis=1)[:,None])
            yanom = (y - y.mean(axis=1)[:,None])
            varxy = np.sqrt((xanom*xanom).sum(1) * (yanom*yanom).sum(1))[:,None]
        else:
            if x.shape[1] != y.shape[1]:
                raise ValueError, "Arrays should have the same width!"
            xanom = (x - x.mean(axis=0))
            yanom = (y - y.mean(axis=0))
            varxy = np.sqrt((xanom*xanom).sum(0) * (yanom*yanom).sum(0))
        Fx = np.fft.fft(xanom, npad, axis=axis)
        Fy = np.fft.fft(yanom, npad, axis=axis)
        iFxy = np.fft.ifft(Fx.conj()*Fy,n=npad,axis=axis).real
    if norm:
        corr_fft = iFxy/varxy
    else:
        corr_fft = iFxy
    #--------
    #arrange in usual form the ccf and determine the time lag
    ccf_positive = corr_fft[:np.size(corr_fft)/2-1]
    ccf_negative = corr_fft[(np.size(corr_fft)/2)-1:]
    corr = np.append(ccf_negative,ccf_positive)
    lag  = np.linspace(-np.size(corr_fft)/2-1,(np.size(corr_fft)/2)-2,np.size(corr_fft))
    #
    #set the range of tau
    if taurange != []:
    	index = np.where(np.logical_and(lag>=taurange[0], lag<=taurange[1]))
	lag=lag[index]
	corr=corr[index]
    return corr,lag
#=============================================================================
def ccf_def(x,y):
    """
    Computes the crosscorrelation of two flat arrays `x` and `y`, with the
    numpy.correlate function.
    Note that the computations are performed on deviations from
    average).
    """
    if x.ndim > 1:
        x = x.ravel()
    if y.ndim > 1:
        y = y.ravel()
    (xanom, yanom) = (x-x.mean(), y-y.mean())
    corxy = np.correlate(xanom, yanom, 'full')
    n = min(x.size, y.size)
    #    return N.r_[ xc[len(yf)-1:], 0, xc[:len(yf)-1] ]
    corxy = np.r_[ corxy[:n][::-1], 0, corxy[n:][::-1] ]
    varxy = np.sqrt(np.inner(xanom,xanom) * np.inner(yanom,yanom))
    corr_def = corxy/varxy
    #--------
    #arrange in usual form the ccf and determine the time lag
    ccf_positive = corr_def[:np.size(corr_def)/2-1]
    ccf_negative = corr_def[(np.size(corr_def)/2)-1:]
    corr = np.append(ccf_negative,ccf_positive)
    lag  = np.linspace(-np.size(corr_def)/2-1,(np.size(corr_def)/2)-2,np.size(corr_def))
    #
    return corr,lag
#=============================================================================
def butter_bandpass(lowcut=1e3, highcut=2e5, fs=2e6, order=5):
    '''
    Returns the a and b coefficients of the IIR Butterworth filter.

    INPUT:
    --------
        lowcut:  the lower frequency cutoff in Hz
        highcut: the upper frequency cutoff in Hz
        fs:      the sampling frequency in Hz
        order:   the order of the filter
    OUTPUT:
    --------
        b: the coeff. of the numerator in the difference eq.
        a: the coeff. of the denominator in the difference eq.
    EXAMPLE:
    --------
        from scipy import signal
        lowcut = 1.0e3
        highcut= 10.0e3
        fs     = 1e6
        order  = 5
        b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    '''
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='bandpass')
    return b, a

#=============================================================================
def iir_bandpass_filter(data, lowcut=1e3, highcut=2e5, fs=2e6, order=5):
    '''
    Filters out data according the the band defined by lowcut and highcut using
    a IIR Butterworth filter.
    '''
    b, a = butter_bandpass(lowcut=lowcut, highcut=highcut, fs=fs, order=order)
    y = lfilter(b, a, data)
    return y,b,a
#=============================================================================
# FIR (finite impulse response)
def bandpass_firwin(ntaps, lowcut, highcut, fs, window = 'hamming'):
    '''
    calculates the coefficients for a FIR filter using windowing
    INPUT:
    --------
        ntaps: Length of the filter (number of coefficients, i.e. the filter order + 1).
               numtaps must be even if a passband includes the Nyquist frequency.
        lowcut: the lower frequency cutoff in Hz
        highcut:the upper frequency cutoff in Hz
        fs:     the sampling frequency in Hz
        window: Desired window to use. See scipy.signal.get_window
                for a list of windows and required parameters.

    OUTPUT:
    --------
        b: Coefficients of length numtaps FIR filter.
    EXAMPLE:
    ________
    '''
    nyq = 0.5 * fs
    b   = signal.firwin(ntaps, [lowcut, highcut], nyq=nyq, pass_zero=False,
                  window=window, scale=False)
    return b
#=============================================================================
def iir_highpass_filter(data,lowcut=1e3,fs=2e6,df=8e2,gpass=25,gstop=62):
    '''
    CHEBYSHEV TYPE 2 IIR HIGHPASS  FILTER:
    '''
    ws    = 2*lowcut/fs
    wp    = 2*df/fs + ws

    b, a = signal.iirdesign(wp, ws, gpass, gstop, analog=False, ftype='cheby2', output='ba')
    y    = signal.lfilter(b,a, data)
    return y

#=============================================================================
def fir_bandpass_filter(data,N,lowcut,highcut,fs,window='hamming'):
    '''
    This function makes the acctual filtering using coefficents calculated by
    bandbass_firwin function.
    '''
    b = bandpass_firwin(N, lowcut, highcut, fs=fs)
    y = signal.lfilter(b,1.0,data)
    #y = signal.filtfilt(b,1.0,data)
    return y,b
#=============================================================================
# AUTO-POWER SPECTRAL DENSITY
def aver_psd(psd_orig,frequency,lin_freq,f0,df0):
    '''
    This function calculates the averaged psd according to the log spaced
    frequency scale calculated by the logspace_freq function.
    INPUT:
    --------
        psd_orig : the original evenly sampled spectrum.
        frequency: the not evenly sampled frequency created by logspace_freq function.
        lin_freq : the original evenly spaced frequency vector.
        f0       : starting frequency needed by logspace_freq.
        df0      : frequency resolution at f0.
    OUTPUT:
    --------
        psd: the averaged auto power spectral density.

    EXAMPLE:
    --------
    '''
    psd = np.zeros(frequency.size)
    for i in range(frequency.size):
        if i == 0:
            ind    = np.where(np.logical_and(lin_freq>f0-df0, lin_freq<=frequency[i]))
            #print ind
            psd[i] = np.mean(psd_orig[ind])
        else:
            ind    = np.where(np.logical_and(lin_freq>frequency[i-1], lin_freq<=frequency[i]))
            psd[i] = np.mean(psd_orig[ind])
    return psd
#=============================================================================
def logspace_freq(fs,f0,df0):
    '''
    This function calculates a frequency scale which is evanly spaced in log scale.
    For the low frequency part, at f0, the frequency resolution is given by df0.
    The frequency changes according to the following formula:

        f[i] = f[i-1]**2 / f[i-2],

    where f[0] = f0 and f[1] = f0 + df0, so the
    first frequency to be calculated is f[2] = f[1]**2 / f[0].
    INPUT:
    --------
    OUTPUT:
    --------
    EXAMPLE:
    --------
    '''
    frequency = [f0,f0+df0]
    i = 1
    while f0 <= fs/2:
        i += 1
        f0 = frequency[i-1]**2/frequency[i-2]
        frequency.append(f0)
    return np.array(frequency)
#=============================================================================

def apsd(x,fs=2e6,df = 200,normalization=False,logspace=False,f0=1e3,df0=200,printinfo=False):
    '''
    Calculates the Auto-Power Spectral Density (APSD) for a signal given in vector x.
    INPUT:
          x:  input signal (N point long)
	  fs: sampling frequency in Hz.
	  df: frequency resolution in Hz.
	  normalization: if True, gets real power density in [V^2/Hz].
	  logsapce:      if True, gets evenly spaced logaritmic scale.
	  f0:            (if logspace=True) gives starting freq. for the logspace axis.
	  df0:           (if logspace=True) gives starting freq.resolution  for the logspace axis.
	  printinfo   :  if True prints informations on the screen.

    '''
    N       = x.size    # total number of points
    M       = int(fs/df)# number of points in a block
    n_block = int(N/M)  # number of blocks for averaging
    norm_factor = 2.0/(df*M**2)# In the normalization factor: 2     - accounts for + and - frequencies
                               #                              1/M^2 - accounts for proper DC value
                               #                              df    - accounts for the finite freq. resolution
    if M>N:
        print "PLEASE REDUCE  FREQUENCY RESOLUTION!"
	print "Freq. resolution should  be at least: df= "+str(int(fs/N))+" Hz"
	return

    if logspace == False:
        apsd_array = np.zeros((n_block,M/2+1))
    else:
        frequency  = logspace_freq(fs,f0,df0)
        apsd_array = np.zeros((n_block,frequency.size))

    if printinfo:
        print "=========== apsd info =========="
        print "Total no. of data: "+str(N)
        print "No. of points/block: "+str(M)
        print "No. of blocks: "+str(n_block)
        print "Shape of the apsd_array: "+str(np.shape(apsd_array))
        if normalization:
            print "Normalization factor for fft: "+str(norm_factor)
            print "APSD unit [V^2/Hz]"
        print "================================"

    for block in range(n_block):
       #print "Block: "+str(block)
        z = x[block*M:(block+1)*M]
        y = signal.detrend(z)
        if normalization:
            psd_y   = norm_factor*np.abs(np.fft.rfft(y))**2
        else:
            psd_y   = np.abs(np.fft.rfft(y))**2
        n_psd   = psd_y.size
        #phase_y = np.angle(np.fft.rfft(y))
        if logspace == False:
            frequency = np.arange(n_psd)*df
            psd = psd_y
        elif logspace == True:
            frequency  = logspace_freq(fs,f0,df0)
            lin_freq   = np.array([f*df for f in range(n_psd)])
            psd        = aver_psd(psd_y,frequency,lin_freq,f0,df0)
        else:
            "logspace should have the value of True or False"

        apsd_array [block,:] = psd

    P_xx = np.mean(apsd_array,axis=0)
    apsd_error = np.std(apsd_array,axis=0)/np.sqrt(n_block)
        #print "Frequency vector has shape: "+str(np.shape(frequency))
    return P_xx,frequency,apsd_error

#=============================================================================
def signal_splitter(x,n=1):
    '''
    By. A. Bencze 2015.
    This function splits a signal x into smaller, n point long intervals
    '''
    N       = len(x)
    m       = int(N/n)
    new_len = m*n
    x_m     = x[0:new_len]
    res   = np.zeros((m,n))
    index = np.arange(m)
    split = np.split(x_m,m)

    for i in index:
        res[i,:] = split[i]
    return res
#=============================================================================
def rescaled_pdf(x,bins=50):
    '''
    By A. Bencze 2015
    This function calculates the rescaled PDF for the time trace x
    '''

    y = (x - np.mean(x))/np.std(x)
    (pdf,bin1) = np.histogram(y,bins=bins)
    return pdf,bin1[1:]
#=============================================================================
def skewness_profile(shot,source='compass-beam',trange=[1.1,1.11],\
                  channels=np.arange(1,19),t0=0.8,fs=2e6, polarity=1,\
                  plot=True,show_ch_1=10,show_ch_2=15,x_range=[1.1,1.11],\
                  filt=False,filt_range=[1e3,30e3],filt_order=10):
    '''
    Returns the skewness profile averaged over a time interval given by 'trange'.
    INPUT:
    --------
        shot:     shot number
        source:   data source see get_rawsignal from the prepare_data module.
        trange:   time range for profile calculation.
        channels: 1d numpy array of channel no. we use for profile calculation.
        t0:       trigger time in sec.
        fs:       sampling frequency in Hz.
        plot:     if True a plot is produced.
        show_ch:  if plot=True, denotes the ploted channel.
        tlim:     time range for the ploted signal.
    OUTPUT:
    --------
        prof:     1d array of profile data, the same length as channels.
    '''
    prof    = np.empty(len(channels))
    for i,channel in enumerate(channels):
        data1 = pd.get_rawsignal(shot,source=source,channel=channel,offset=True,t1=trange[0],t2=trange[1])
        if filt:
            (data1,b) = fir_bandpass_filter(data1,filt_order,filt_range[0],filt_range[1],fs,window='hamming')
        trend_data = smooth(data1,window_len=500)
        data1 = data1 - trend_data
	prof[i]  = skew(data1[200:])
    if plot:
        fig = plt.figure()
        ax1 = fig.add_subplot(311)
        ax1.plot(channels,prof,'--o',markersize=8,color='red',label='t=[%1.5f , %1.5f s]'%(trange[0],trange[1]))
        ax1.axhline(y=0,color='black',linewidth=1)
        ax1.set_xlabel("channel")
        ax1.set_ylabel("Skewness [a.u.]")
        ax1.legend(loc='best')

        ax2 = fig.add_subplot(312)
        data2 = pd.get_rawsignal(shot,source=source,channel=show_ch_1,offset=True,t1=trange[0],t2=trange[1])
        if filt:
            (data2,b) = fir_bandpass_filter(data2,filt_order,filt_range[0],filt_range[1],fs,window='hamming')
        trend_data = smooth(data2,window_len=500)
        data2 = data2 - trend_data
        m1     = np.mean(data2)
        sigma1 = np.std(data2)
        time = np.array([t*(1/fs) for t in range(data2.size)])+trange[0]
        ax2.plot(time[200:],data2[200:]*1000,linewidth=2,label='#'+str(shot)+'/CH:'+str(show_ch_1))
        #ax2.plot(time[200:],trend_data[200:]*1000,'k-',label='trend')
        ax2.axhline(y=m1*1000,color='black',linewidth=1)
        ax2.axhline(y=m1*1000+sigma1*1000,color='red',linestyle='--',linewidth=1)
        ax2.axhline(y=m1*1000-sigma1*1000,color='red',linestyle='--',linewidth=1)
        ax2.set_xlim(x_range)
        ax2.legend()
        ax2.set_xlabel("time [s]")
        ax2.set_ylabel("Signal [mV]")

        ax3 = fig.add_subplot(313)
        data3 = pd.get_rawsignal(shot,source=source,channel=show_ch_2,offset=True,t1=trange[0],t2=trange[1])
        if filt:
            (data3,b) = fir_bandpass_filter(data3,filt_order,filt_range[0],filt_range[1],fs,window='hamming')
        trend_data = smooth(data3,window_len=500)
        data3 = data3 - trend_data
        m2     = np.mean(data3[200:])
        sigma2 = np.std(data3[200:])
        time = np.array([t*(1/fs) for t in range(data3.size)])+trange[0]
        ax3.plot(time[200:],data3[200:]*1000,linewidth=2,label='#'+str(shot)+'/CH:'+str(show_ch_2))
        #ax3.plot(time[200:],data2[200:]*1000,label='#'+str(shot)+'/CH:'+str(show_ch_1))
        ax3.axhline(y=m2*1000,color='black',linewidth=1)
        ax3.axhline(y=m2*1000+sigma2*1000,color='red',linestyle='--',linewidth=1)
        ax3.axhline(y=m2*1000-sigma2*1000,color='red',linestyle='--',linewidth=1)
        ax3.set_xlim(x_range)
        ax3.legend()
        ax3.set_xlabel("time [s]")
        ax3.set_ylabel("Signal [mV]")
        plt.show()
    return prof
#=============================================================================
def rms(x,axis=None):
    return np.sqrt(np.mean(x**2,axis = axis))
#=============================================================================
def rel_fluc_ampl(shot,source='compass-beam',polarity=-1,channel=13,trange_on=[1.134,1.154],\
                  trange_off=[1.025,1.045],t0=0.8,fint_noise=[1e5,5e5],fs=2e6,f0=1e3,\
                  df=200,printinfo=False):
    '''
    This function calculates the relative fluctuation amplitude profile of the
    BES signal by means of the power spectral density using both the on_time and
    the off_time data.
    The procedure goes like this:
        1. Calculate the integrated power for the noise in the frequency
           range 'fint_noise'. (for both on_time and off_time)
        2. Subtrac the noise power from the total power getting the
           fluctuation power
        3. From the on_time fluctuation power subtract the off_time
           fluctuation power,
           this gives the fluctuation power originating only from the Li-beam.
        4. Calculate the relative fluctuation amplitude (rfa) as the ratio
           of the square root of the fluctuation power (this is basically
           the RMS amplitude)and the mean value of the beam-signal
           (background subtracted).
    '''

    data_on  = pd.get_rawsignal(shot,source=source,polarity=polarity,channel=channel,\
                                t1=trange_on[0],t2=trange_on[1],t0=t0)
    data_off = pd.get_rawsignal(shot,source=source,polarity=polarity,channel=channel,\
                                t1=trange_off[0],t2=trange_off[1],t0=t0)
    (P_on,f_on,err_on)    = apsd(data_on,fs,f0=f0,df=df,normalization=True)
    (P_off,f_off,err_off) = apsd(data_off,fs,f0=f0,df=df,normalization=True)

    index_on         = np.where(np.logical_and(f_on>=fint_noise[0],\
                                f_on<=fint_noise[1]))
    noise_lev_on     = np.mean(P_on[index_on])
    index_off        = np.where(np.logical_and(f_off>=fint_noise[0],\
                                f_off<=fint_noise[1]))
    noise_lev_off    = np.mean(P_off[index_off])
    noise_power_on   = noise_lev_on*(fs/2-f0)
    noise_power_off  = noise_lev_off*(fs/2-f0)
    total_power_on   = np.sum(P_on)*df
    total_power_off  = np.sum(P_off)*df
    fluc_power_on    = total_power_on - noise_power_on
    fluc_power_off   = total_power_off - noise_power_off
    fluc_power       = fluc_power_on - fluc_power_off
    rfa = np.sqrt(fluc_power)/(np.mean(data_on)-np.mean(data_off))
    if printinfo:
        print '=======Rel.fluc.ampl. info======'
        print 'Noise power ON:  ' +str(noise_lev_on)
        print 'Noise power OFF: ' +str(noise_lev_off)
        print 'Total power ON:  ' +str(total_power_on)
        print 'Total power OFF: ' +str(total_power_off)
        print 'Fluc. power ON:  ' +str(fluc_power_on)
        print 'Fluc. power OFF: ' +str(fluc_power_off)
        print 'Fluc. power:     ' +str(fluc_power)
        print 'Rel. Fluc. Ampl.:' +str(rfa)+'%'
        print '================================'
    if fluc_power < 0:
        rfa = 0
    return rfa


#=============================================================================
def rt_correlation(shot,source='compass-beam',t0=0.8,fs=2e6,polarity=-1,\
                   offset=True,trange=[0.8,1.8],taurange=[-100,100],\
                   ref_channel=11,channels=np.arange(1,19),filt=False,\
                   filt_order=100,lowcut=1e3,highcut=5e5,photoncut_range=[2,5],\
                   norm=False):
    '''
    Calculates the spatial-temporal correlation along the beam
    '''
    corr_array = np.zeros((taurange[1]*2+1,len(channels)))
    ref_data = pd.get_rawsignal(shot,source=source,t0=t0,fs=fs,offset=offset,\
              polarity=polarity,t1=trange[0],t2=trange[1],channel=ref_channel)
    for i,plot_channel in enumerate(channels):
        print 'calc. correlation of ch  #'+str(ref_channel)+' & ch #'+str(plot_channel)
        plot_data = pd.get_rawsignal(shot,source=source,t0=t0,fs=fs,offset=offset,\
                    polarity=polarity,t1=trange[0],t2=trange[1],channel=plot_channel)
        if filt:
            ref_data_filt,b  = fir_bandpass_filter(ref_data,filt_order,lowcut,highcut,fs,window='hamming')
            plot_data_filt,b = fir_bandpass_filter(plot_data,filt_order,lowcut,highcut,fs,window='hamming')
        else:
            ref_data_filt  = ref_data
            plot_data_filt = plot_data

        x = signal.detrend(ref_data_filt,type='linear')
        y = signal.detrend(plot_data_filt,type='linear')

        if filt==False and plot_channel==ref_channel:
            acf,lag_acf = ccf(x,y,norm=norm,taurange=taurange)
            acf_corrected,fluc_ampl,ph_ampl = photon_cut(acf,lag,interp_range=photoncut_range)
            print '->->->- photon noise cut done'
            plt.figure()
            plt.plot(lag,acf_corrected,'-r')
            corr_array[:,i] = acf_corrected
        else:
            corr_array[:,i],lag = ccf(x,y,norm=norm,taurange=taurange)
    return corr_array,channels,lag
#=============================================================================
def std_profile(shot,source='compass-beam',digi_res=12,t0=0.8,fs=2e6,polarity=-1,\
                offset=True,trange=[0.8,1.8],\
                channels=np.arange(1,19),filt=False,\
                filt_order=100,lowcut=1e3,highcut=5e5):
    '''
    Calculates the profile of the absolute fluctuation level.
    '''
    fluc_level_prof = np.zeros(len(channels))
    for i,channel in enumerate(channels):
        print 'channel: #'+str(channel)
        raw_data = pd.get_rawsignal(shot,source=source,fs=fs,digi_res=digi_res,polarity=polarity,\
                                t0=t0,channel=channel,t1=trange[0],t2=trange[1],offset=offset)
        if filt:
            (data,b) = fir_bandpass_filter(raw_data,filt_order,lowcut,highcut,fs)
        else:
            data = raw_data
        fluc_level_prof[i] = np.std(data)
    return fluc_level_prof,channels
#=============================================================================
def spectral_exponent(shot,source='compass-beam',fs=2e6,digi_res=12,polarity=1,\
                      t0=0.8,trange=[1.11,1.2],channel=11,offset=True,fit_frange=[1e4,4e5],\
                      norm_spectra = True):
    '''
    This function fits a linear function in log-log space to the power spectral
    density in the frequency range given by "fit_frange". The outputs are the
    spectral exponent (slope), the freqencies for the fitted curve and the fitted
    curve itself.
    '''

    data = pd.get_rawsignal(shot,source=source,fs=fs,digi_res=digi_res,\
                            polarity=polarity,t0=t0,t1=trange[0],t2=trange[1],\
                            channel=channel,offset=offset)
    pxx,f,err = apsd(data,normalization=norm_spectra)

    index = np.where(np.logical_and(f>fit_frange[0],f<fit_frange[1]))
    freq  = f[index]
    psd   = pxx[index]
    logPxx = np.log10(psd)
    logF   = np.log10(freq)
    # the coefficients for the linear plot in the log-log space
    coeff = np.polyfit(logF,logPxx,1)
    # transforming back to linear space using the coeff's
    fit_curve    = 10**coeff[1]/freq**np.abs(coeff[0])

    spectral_exp = coeff[0]
    return spectral_exp,freq,fit_curve,pxx,f,err
#=============================================================================

    '''
    Read data with the get_rawsignal, or from an array
    INPUT: same as get_rawsignal or a data array
    OUTPUT: a data array
    '''
def data_read(data=[], shot=10718, channel=12, source="compass-beam", fs=2e6 ,\
              digi_res=14):
    if data == []:
        data=pd.get_rawsignal(shot,source=source,channel=channel,offset=True,\
                              fs=fs,digi_res=digi_res,printinfo=False)
    else:
        data=data
    return data


#=============================================================================


def remove_peak(shot=10718, channel=12, timerange=[0.912,1.91], prec=100, sens=1E3,\
                save_to_file=False, source="compass-beam", fs=2e6,digi_res=14,\
                yrange=[0,0], aftersens=10, t0=0.912, data=[]):

    '''
    Removes the peaks from a data array
    INPUT: Same as get_rawsignal and
           prec: precision of the peak detecting: the area size around the
	         peak which determinate the mean and the STD
	   sens: sensitivity of the peak detection: the peak need to be higher
	         than the precision area mean + sens*precision area STD
	   aftersens: the multiplier which tells how much important the
	         precision area after the peak: it means peek need
		 to be higher than precision area
		 after peak mean +sens/aftersens * precision are after peak STD
           yrange: plotted y range
	   data: the data array
    OUTPUT: data array
    '''

    data=peakUI(shot=shot,channel= channel, timerange=timerange, prec=prec, sens=sens,\
                save_to_file=False, source=source, fs=fs,digi_res=digi_res, yrange=yrange,\
                aftersens=aftersens, t0=t0, data=data)
    return data


#This function change the data around the peak
def peakRemoving(data, hights, area, hightsBeforeMean, hightsBeforeSTD):
    length=len(hights)
    for i in range(0,length):
        for k in range(-area,area):
            data[hights[i]+k]=hightsBeforeMean[i]+random.uniform(-hightsBeforeSTD[i],hightsBeforeSTD[i])
    return data

#This function can detect the peaks
def peakDetection(data,time,index,sens, prec, aftersens):
        global removePeakProcessPercentage
        positionCorrection=0
        precBefore=np.zeros(prec)
        precAfter=np.zeros(prec)
        hights=np.empty(0)
        hightsTime=np.empty(0)
        hightsData=np.empty(0)
        hightsBeforeMean=np.empty(0)
        hightsBeforeSTD=np.empty(0)
        hightsAfterMean=np.empty(0)
        hightsAfterSTD=np.empty(0)
        length=len(index)

        #Identifi the peaks
	startIndex=0
        loopStart=index[0]
	loopEnd=index[length-1]-prec
	loopLength=loopEnd-loopStart
	if loopLength < prec:
	    print 'REMOVE_PEAK>Precision(' + str(prec) + ') is too long for this data range, please set precision to maximum: ' + str(loopLength)
        for i in range(loopStart,loopEnd):
	    startIndex=i-index[0]
            if ((startIndex) != 0) and (((startIndex) % prec)==0):
	        positionCorrection=positionCorrection+1

	    if startIndex <prec:
	        precBefore[startIndex]=data[i]
	    else: #Calculate only if the precBefore have enought(prec) data points
                if startIndex  == prec:
	            for k in range(0,prec):
		        precAfter[k]=data[i+1+k]
                elif startIndex  > prec:
	            if startIndex-1-prec*positionCorrection == -1:
		        precAfter[prec-1] = data[i+prec]
		    else:
		        precAfter[startIndex-1-prec*positionCorrection] = data[i+prec]

	        if startIndex -1-prec*positionCorrection == -1:
	            precBefore[prec-1]=data[i-1]
	        else:
                    precBefore[startIndex-1-prec*positionCorrection]=data[i-1]

#               precBeforeMean=np.mean(precBefore)
#	        precBeforeSTD=np.std(precBefore)
#               precAfterMean=np.mean(precAfter)
#	        precAfterSTD=np.std(precAfter)

		precBeforeCopy=np.copy(precBefore)
		precAfterCopy=np.copy(precAfter)

		#Calculate the precBeforeMean
                precBeforeMean=0
		for k in range(0,prec):
		    precBeforeMean+=precBeforeCopy[k]
		precBeforeMean=precBeforeMean/prec

		#Calculate the precBeforeSTD
                precBeforeSTD=0
		for k in range(0,prec):
		    precBeforeCopy[k]=(precBeforeCopy[k]-precBeforeMean)*(precBeforeCopy[k]-precBeforeMean)
		precBeforeSTDMean=0
		for k in range(0,prec):
		    precBeforeSTDMean+=precBeforeCopy[k]
		precBeforeSTDMean=precBeforeSTDMean/prec
		precBeforeSTD=(precBeforeSTDMean)**(1./2.)


		#Calculate the precAfterMean
                precAfterMean=0
		for k in range(0,prec):
		    precAfterMean+=precAfterCopy[k]
		precAfterMean=precAfterMean/prec

		#Calculate the precAfterSTD
                precAfterSTD=0
		for k in range(0,prec):
		    precAfterCopy[k]=(precAfterCopy[k]-precAfterMean)*(precAfterCopy[k]-precAfterMean)
		precAfterSTDMean=0
		for k in range(0,prec):
		    precAfterSTDMean+=precAfterCopy[k]
		precAfterSTDMean=precAfterSTDMean/prec
		precAfterSTD=(precAfterSTDMean)**(1./2.)

                removePeakProcessPercentage=startIndex/loopLength

		#Detecting peaks
                if data[i] > precBeforeMean+sens*precBeforeSTD and data[i] > precAfterMean+sens*precAfterSTD/aftersens:
	            hights=np.append(hights,i)
		    hightsTime=np.append(hightsTime,time[i])
		    hightsData=np.append(hightsData,data[i])
                    hightsBeforeMean=np.append(hightsBeforeMean,precBeforeMean)
                    hightsBeforeSTD=np.append(hightsBeforeSTD,precBeforeSTD)
        return hights,hightsTime,hightsData,hightsBeforeMean,hightsBeforeSTD

#This is the UI for removing peak
def peakUI(shot=10718, channel=12, timerange=[0.912,1.91], prec=100, sens=1E3, save_to_file=False,\
           source="compass-beam", fs=2e6,digi_res=14, yrange=[0,0], aftersens=10, t0=0.912, data=[]):

    print ' '
    print 'REMOVE_PEAK>Program started'
    print 'REMOVE_PEAK>Reading data.....'

    timer_start=times.time()
    #Reading data for get-rawsignal, or a single np.array form outside
    data=data_read(shot=shot,channel=channel, source=source, fs=fs,digi_res=digi_res, data=data)
    timer_end=times.time()

    time = np.array([t*1/fs for t in range(len(data))]) + t0
    area=int(prec/10)+1

    ##print 'REMOVE_PEAK>Reading data tooks ' + str(timer_end-timer_start) + ' s'


    if yrange[0]==0 and yrange[1]==0:
        yrange[0]=0.9*min(data)
	yrange[1]=1.1*max(data)

    conti=1
    while conti==1:

        index = np.where(np.logical_and(time>=timerange[0], time<=timerange[1]))
        index=index[0]
        timer_start=times.time()

        print 'REMOVE_PEAK>Peak detecting.....'

    	#Peak detection
        detectedPeaks=peakDetection(data,time,index,sens,prec,aftersens)
        hights=detectedPeaks[0]
        hightsTime=detectedPeaks[1]
        hightsData=detectedPeaks[2]
        hightsBeforeMean=detectedPeaks[3]
        hightsBeforeSTD=detectedPeaks[4]
        timer_end=times.time()

        ##print 'REMOVE_PEAK>Processing tooks ' + str(timer_end-timer_start) + 's'
        print 'REMOVE_PEAK>' + str(len(hights)) + ' peaks found'

        #Plot
        plt.ion()
	plt.clf()
	plt.plot(time, data)
	plt.title(str(shot) + ' APD-' + str(channel))
	plt.xlabel('time [s]')
	plt.axis([timerange[0],timerange[1],yrange[0],yrange[1]])
	if len(hights) != 0:
	    plt.plot(hightsTime, hightsData, 'ro')
	plt.draw()

        #UI section
	print 'REMOVE_PEAK>To cut the founded peaks please type \'c\'(cut), if you want to redefinate the parameters please type \'r\', change time intervals, tpye \'t\' or change parameters and time intervals at the same time, type \'rt\'. If you want to type \'q\' to quit.'
	get_answ=0
	while get_answ==0:
	    answ=raw_input('REMOVE_PEAK>Answer: ')
	    if answ == 'c':
		data=peakRemoving(data, hights, area, hightsBeforeMean, hightsBeforeSTD)
                print 'REMOVE_PEAK>Peaks cutted'

		#Plot cutted signal
                plt.clf()
	        plt.plot(time,data)
	        plt.title(str(shot) + ' APD-' + str(channel))
	        plt.xlabel('time [s]')
	        plt.axis([timerange[0],timerange[1],yrange[0],yrange[1]])
		plt.draw()

	        print 'REMOVE_PEAK>To start finding peaks with the same parameters please type \'a\'(again), if you want to redefinate the parameters please type \'r\', change time intervals, tpye \'t\' or change parameters and time intervals at the same time, type \'rt\'. If you want to type \'q\' to quit.'
	        while get_answ==0:

	             answ=raw_input('REMOVE_PEAK>Answer: ')
	             if answ == 'a':
	                 get_answ=1
                         conti=1
	             elif answ == 'r':
	                 get_answ=1
		         conti=1
	                 sens=float(raw_input('REMOVE_PEAK>Sensitivity(sens)='))
		         prec=int(raw_input('REMOVE_PEAK>Precision(prec)='))
	             elif answ == 't':
	                 get_answ=1
		         conti=1
	                 timerange[0]=float(raw_input('REMOVE_PEAK>Start time='))
		         timerange[1]=float(raw_input('REMOVE_PEAK>End time='))
	             elif answ == 'rt':
	                 get_answ=1
		         conti=1
		         timerange[0]=float(raw_input('REMOVE_PEAK>Start time='))
		         timerange[1]=float(raw_input('REMOVE_PEAK>End time='))
	                 sens=float(raw_input('REMOVE_PEAK>Sensitivity(sens)='))
		         prec=int(raw_input('REMOVE_PEAK>Precision(prec)='))
	             elif answ == 'q':
	                 get_answ=1
	                 conti=0
	             else:
	                 print 'REMOVE_PEAK>Please type \'a\',\'r\' or \'q\''
	    elif answ == 'r':
	        get_answ=1
		conti=1
	        sens=float(raw_input('REMOVE_PEAK>Sensitivity(sens)='))
		prec=int(raw_input('REMOVE_PEAK>Precision(prec)='))
	    elif answ == 't':
	        get_answ=1
		conti=1
		timerange[0]=float(raw_input('REMOVE_PEAK>Start time='))
		timerange[1]=float(raw_input('REMOVE_PEAK>End time='))
	    elif answ == 'rt':
	        get_answ=1
		conti=1
		timerange[0]=float(raw_input('REMOVE_PEAK>Start time='))
		timerange[1]=float(raw_input('REMOVE_PEAK>End time='))
	        sens=float(raw_input('REMOVE_PEAK>Sensitivity(sens)='))
		prec=int(raw_input('REMOVE_PEAK>Precision(prec)='))
	    elif answ == 'q':
	        get_answ=1
	        conti=0
	    else:
	        print 'REMOVE_PEAK>Please type \'c\',\'r\' or \'q\''

    plt.show()
    print 'REMOVE_PEAK>Program closed'
    return data
#============================================================================

def select_time(shot=10718, channel=12, timerange=[0.912, 1.91], save=False, \
                source="compass-beam", fs=2e6, digi_res=14, data=[], \
                level=float('Inf'), t0=0.912, delay=0.01, min_interval=0.1, GUI=True, path=0, decim=False):
    '''
    Select on and off times from a chopped data file
    INPUT: same as get_rawsignal
           data: if you aleready have a data, you dont have to use get_rawsignal,
	         just put the data array here
	   level: for not atomatic calculatig the level, you can add to this the value which
	         tells what data array is in up stat and what is in down
           delay: the half of the min range between two states [ms]
	   min_interval: the minimum intervallum whic can be detected a state [ms]
    OUTPUT: a times[] array:
            times[0]: on times start
	    times[1]: on times end
	    times[2]: off times start
	    times[3]: off times end
    '''

    if GUI:
        times = select_time_UI(shot=shot, channel=channel, timerange=timerange, \
                               save=save, source=source, fs=fs, digi_res=digi_res, \
                               data=data, level=level, t0=t0, delay=delay, \
                               min_interval=min_interval, path=path, decim=decim)
    else:
        times = select_time_run(shot=shot, channel=channel, timerange=timerange, \
                                save=save, source=source, fs=fs, digi_res=digi_res, \
                                data=data, level=level, t0=t0, delay=delay, \
                                min_interval=min_interval, path=path, decim=decim)
    return times


from scipy import signal as sg


def select_time_run(shot=10718, channel=12, timerange=[0.912, 1.91], save=False, \
                    source="compass-beam", fs=2e6, digi_res=14, data=[], \
                    level=float('Inf'), t0=0.912, delay=0.01, min_interval=0.1, path=0, decim=True):
    saved = save
    # Preparing data
    data = data_read(data=data, shot=shot, channel=channel, source=source, fs=fs, digi_res=digi_res)
    time = np.array([t * 1 / fs for t in range(len(data))]) + t0
    if decim == True:
        data = sg.decimate(data, 50)
        dt = 1.0 / (data.size - 1)
        time = np.array([t * dt for t in range(np.size(data))]) + t0
    index = np.where(np.logical_and(time >= timerange[0], time <= timerange[1]))
    index = index[0]

    edges = []
    times = selecting_times(data=data, time=time, index=index, level=level, delay=delay, \
                            min_interval=min_interval, edges=edges)
    on_times_start_final = times[0]
    on_times_end_final = times[1]
    off_times_start_final = times[2]
    off_times_end_final = times[3]

    # Write to file
    if saved == True:
        select_time_write_to_file(shot, channel, on_times_start_final, on_times_end_final, \
                                  off_times_start_final, off_times_end_final, save, path=path)

    return on_times_start_final, on_times_end_final, off_times_start_final, off_times_end_final


def select_time_UI(shot=10718, channel=12, timerange=[0.912, 1.91], save=False, \
                   source="compass-beam", fs=2e6, digi_res=14, data=[], \
                   level=float('Inf'), t0=0.912, delay=0.01, min_interval=0.1, path=0, decim=True):
    print ' '
    print 'SELECT_TIME>Program started.'
    print 'SELECT_TIME>Reading data....'

    # Preparing data
    data = data_read(data=data, shot=shot, channel=channel, source=source, fs=fs, digi_res=digi_res)
    time = np.array([t * 1 / fs for t in range(len(data))]) + t0
    if decim == True:
        data = sg.decimate(data, 50)
        dt = 1.0 / (data.size - 1)
        time = np.array([t * dt for t in range(np.size(data))]) + t0
    index = np.where(np.logical_and(time >= timerange[0], time <= timerange[1]))
    index = index[0]

    print 'SELECT_TIME>Selecting times....'
    edges = []
    times = selecting_times(data=data, time=time, index=index, level=level, delay=delay, \
                            min_interval=min_interval, edges=edges)
    on_times_start_final = times[0]
    on_times_end_final = times[1]
    off_times_start_final = times[2]
    off_times_end_final = times[3]
    print 'SELECT_TIME>' + str(len(on_times_start_final)) + ' on times and ' + str(
        len(off_times_start_final)) + ' off times found.'

    # Plot
    plt.clf()
    plt.plot(time, data, 'b')
    plt.draw()
    plt.title(str(shot) + ' APD-' + str(channel))
    plt.xlabel('Time [s]')
    for i in range(len(on_times_start_final)):
        plt.plot([on_times_start_final[i], on_times_end_final[i]], [edges[1] + abs(edges[1] - edges[0]) * 1.15, \
                                                                    edges[1] + abs(edges[1] - edges[0]) * 1.15], 'b',
                 linewidth=1)
    for i in range(len(off_times_start_final)):
        plt.plot([off_times_start_final[i], off_times_end_final[i]], [edges[1] + abs(edges[1] - edges[0]) * 1.05, \
                                                                      edges[1] + abs(edges[1] - edges[0]) * 1.05], 'r',
                 linewidth=1)
    yrange = [edges[0] - abs(edges[1] - edges[0]) * 0.5, edges[1] + abs(edges[1] - edges[0]) * 1.5]
    plt.axis([timerange[0], timerange[1], yrange[0], yrange[1]])
    plt.draw()
    plt.show(block=False)

    # Write to file
    saved = False
    while saved != True:
        save = raw_input('SELECT_TIME>Do you want to save the on/off times to file? (y/n): ')
        if save == 'y' or save == 'n':
            saved = True
            select_time_write_to_file(shot, channel, on_times_start_final, on_times_end_final, \
                                      off_times_start_final, off_times_end_final, save, path=path)
        else:
            print 'SELECT_TIME>Pleas type \'y\' or \'n\'!'

    print 'SELECT_TIME>Program closed'
    print ' '
    return on_times_start_final, on_times_end_final, off_times_start_final, off_times_end_final


def select_time_write_to_file(shot, channel, on_times_start_final, on_times_end_final, \
                              off_times_start_final, off_times_end_final, save, path=False):
    if save == 'y':
        if path != 0:
            on_file_name = path + 'on_times.txt'
            off_file_name = path + 'off_times.txt'
        else:
            on_file_name = './data/' + str(shot) + '/Channel' + str(channel) + '/' + 'on_times.txt'
            off_file_name = './data/' + str(shot) + '/Channel' + str(channel) + '/' + 'off_times.txt'
        on_file = open(on_file_name, 'w+')
        off_file = open(off_file_name, 'w+')
        for i in range(0, len(on_times_start_final)):
            on_file.write(str(on_times_start_final[i]))
            on_file.write('\t')
            on_file.write(str(on_times_end_final[i]))
            on_file.write('\n')
        print 'SELECT_TIME>' + on_file_name + ' is written.'
        for i in range(0, len(off_times_start_final)):
            off_file.write(str(off_times_start_final[i]))
            off_file.write('\t')
            off_file.write(str(off_times_end_final[i]))
            off_file.write('\n')
        print 'SELECT_TIME>' + off_file_name + ' is written.'
        on_file.close()
        off_file.close()
    elif (save == 'n'):
        print 'SELECT_TIME>No time files writte. The time arrays are returned.'


def selecting_times(data=[], time=[], index=[], level=float('Inf'), delay=0, min_interval=[], edges=[]):
    # Making the good scale
    delay = delay * 1E-3
    min_interval = min_interval * 1E-3

    # For the whol data array
    if index == []:
        index = np.arange(0, len(data))

    # aking the edges
    if len(edges) == 0:
        edges = np.append(edges, np.min(data))
        edges = np.append(edges, np.max(data))

    # Defining the level
    if level == float('Inf'):
        level = (edges[0] + edges[1]) / 2

    on_times_start = np.empty(0)
    on_times_end = np.empty(0)
    off_times_start = np.empty(0)
    off_times_end = np.empty(0)

    # Detecting all
    for i in index:
        on = (data[i] > level)
        off = not on
        if i != index[0]:
            on_before = (data[i - 1] > level)
            off_before = not on_before
        # First element
        if i == index[0]:
            if on:
                on_times_start = np.append(on_times_start, time[i])
            else:
                off_times_start = np.append(off_times_start, time[i])
        # Last element
        elif i == index[-1]:
            if on_before:
                on_times_end = np.append(on_times_end, time[i])
            else:
                off_times_end = np.append(off_times_end, time[i])
        # Mid elements
        else:
            if off and on_before:
                on_times_end = np.append(on_times_end, time[i - 1])
                off_times_start = np.append(off_times_start, time[i])
            if on and off_before:
                off_times_end = np.append(off_times_end, time[i - 1])
                on_times_start = np.append(on_times_start, time[i])

    # Select intervals
    on_length = len(on_times_start)
    off_length = len(off_times_start)
    on_times_start_final = np.empty(0)
    off_times_start_final = np.empty(0)
    on_times_end_final = np.empty(0)
    off_times_end_final = np.empty(0)

    for i in range(0, on_length):
        if on_times_end[i] - on_times_start[i] > min_interval:
            on_times_start_final = np.append(on_times_start_final, on_times_start[i] + delay)
            on_times_end_final = np.append(on_times_end_final, on_times_end[i] - delay)
    for i in range(0, off_length):
        if off_times_end[i] - off_times_start[i] > min_interval:
            off_times_start_final = np.append(off_times_start_final, off_times_start[i] + delay)
            off_times_end_final = np.append(off_times_end_final, off_times_end[i] - delay)

    return on_times_start_final, on_times_end_final, off_times_start_final, off_times_end_final

#============================================================================
def deflection_period(shot,source='compass-cdb',channels=np.arange(1,19),tstart=1.1,tstop=1.2,\
		      n_samples_in_period=8,shift=3,fs=2e6,digi_res=14):
    '''
    Plots the average intensity of the deflection period in the channels defined by channels.
    INPUT:
        same as get_rawsignal plus:
	    n_samples_in_period : number of data points in a deflection period.
	    shift: number of sample shifts from
    '''
    fig = plt.figure()
    ax  = fig.add_subplot(111)
    for i,channel in enumerate(channels):
        print 'channel:',channels[i]
        data     = pd.get_rawsignal(shot,source=source,channel=channels[i],offset=True,t1=tstart,\
                   t2=tstop,digi_res=digi_res,fs=fs)
        periods     = signal_splitter(data[shift:],n_samples_in_period)
        mean_period = np.mean(periods,axis=0)
        ax.plot((mean_period-mean_period[3])+i*0.01,'o--r',markersize=8,markerfacecolor='black')
        plt.text(-2,mean_period[0]-mean_period[3]+0.01*i,'ch:'+str(channel))
    ax.set_xlim(-3,10)
    ax.set_xlabel('samples')
    ax.get_yaxis().set_ticks([])
    fig.suptitle('Average deflection period for  shot #'+str(shot), fontsize=14)
    plt.show()
    #return mean_period
#===============================================================================
def photon_cut(acf,lag,interp_range=[1,3]):
    '''
    Cuts the photon noise peak of the autocovariance function by linear
    interpolation around tau=0.
    INPUT:
        acf: the original autocovariance function
        lag: the vector of lags coresponding to the acf
        interp_range: the range in leg units where the linear interpolation is
                      performed
    OUTPUT:
        acov:the photon noise  corrected Auto-covariance.
        fluc_ampl: the squere of rms fluctuation amplitude(acov(tau=0))
        ph_ampl: the squere of the photon noise rms amplitude(acf(tau=0))
    '''
    acov   = acf
    index  = np.where(np.logical_and(lag>=interp_range[0],lag<=interp_range[1]))
    index0 = np.where(lag==0)
    x = lag[index]
    z = acf[index]
    p = np.polyfit(x,z,1)
    ph_ampl      = acf[index0]
    acov[index0] = p[1]
    fluc_ampl = p[1]
    return acov, fluc_ampl, ph_ampl


# ===============================================================================


def counting_blob(data=None, sigma=0, main_sigma=0, data_mean=0, fitting_range=10):
    if data is None:
        data = []

    blob_positions = []

    for index in range(len(data)):
        if (data[index] > sigma * main_sigma + data_mean):
            if index + fitting_range > len(data)-1:
                continue
            elif index - fitting_range < 0:
                continue
            else:
                if len(data) == 0:
                    print index
                if max(data[index - fitting_range:index + fitting_range]) == data[index]:
                    if len(blob_positions) == 0:
                        blob_positions = np.array([index])
                    else:
                        blob_positions = np.concatenate((blob_positions, [index]), axis=0)

    return blob_positions


def removing_blobs(data, time, mean, blob_positions, fitting_range, removable_percent):
    blob_data = np.full(np.size(data), mean)
    blobs_array = np.array([])
    actual_data = np.array([])
    for position in blob_positions:
        if position - fitting_range < 0:
            index = np.arange(0, position + fitting_range)
            continue
        elif position + fitting_range > len(data)-1:
            continue
        else:
            index = np.arange(position - fitting_range, position + fitting_range)
        actual_data = data[index]
        blob_data[index] = np.copy(actual_data)
        special_blobs_array = specialSignalArray()
        special_blobs_array.array = np.copy(actual_data)
        blobs_array = np.append(blobs_array, special_blobs_array)
        height = abs(data[position] - mean)

        for subindex in range(len(actual_data)):
            actual_data[subindex] = mean
        data[index] = actual_data
    return data, blob_data, blobs_array

class specialSignalArray():
    channel = 0
    array = []

    def size(self):
        return np.size(self.array)

def multiply_blob_removing(data, time, sigmas, fitting_range, removable_percent):
    """

    :type sigmas: object
    """
    detection = detectableBlob()
    detection.data = data
    result = np.array([])
    sigma = np.std(data)
    mean = np.mean(data)
    removed_blobs_array = np.array([])
    blobs_array = np.array([])
    blob_positions = np.array([])
    for th in sigmas:
        special_blobs_positions = specialSignalArray()
        removed_blobs=specialSignalArray()
        special_blobs_array = specialSignalArray()
        detection = detectableBlob()
        detection.data = np.copy(data)
        blobs_positions_index = counting_blob(data, th, sigma, mean, fitting_range)
        special_blobs_positions.array = np.copy(blobs_positions_index)
        blob_positions = np.append(blob_positions, special_blobs_positions)
        detection.blobs = np.copy(blobs_positions_index)
        if result.size == 0:
            result = np.array([detection])
        else:
            result = np.append([result], [detection])
        data, removed_blobs.array, blob_array=removing_blobs(data, time, mean, blobs_positions_index, fitting_range, removable_percent)
        removed_blobs_array = np.append(removed_blobs_array, removed_blobs)
        special_blobs_array.array = blob_array
        blobs_array = np.append(blobs_array, special_blobs_array)
    finalDetection = detectableBlob()
    finalDetection.data = np.copy(data)
    result = np.append([result], [finalDetection])
    return result, removed_blobs_array, blobs_array, blob_positions


class detectableBlob():
    data = np.array([])
    blobs = np.array([])



# ===============================================================================

def count_blob_positions(shot=11348, source="compass-local", channel=15, times=[], sigmas=[], fitting_range_msec=0, filter_low = 20, filter_high = 100, filt_order = 5, path = ""):
    # Fitting range is in microsec
    done = False
    fitting_range = 0
    blob_positions = np.array([])
    c_avarage_full = np.array([])
    for time_index in range(len(times[0])):
        data, time, indexes = pd.get_rawsignal(shot=shot, source=source, channel=channel, t1=times[0][time_index],
                                               t2=times[1][time_index],
                                               with_time=True)
        data, B = fir_bandpass_filter(data, filt_order, filter_low, filter_high, 2e6)
        if not done:
            fitting_range = int(
                (len(time) / ((times[1][time_index] - times[0][time_index]) * 1e6) * fitting_range_msec))
            done = True
        sigmas = np.sort(sigmas)[::-1]
        plottable_information, removed_blobs, blobs_array, prepare_blob_positions = multiply_blob_removing(data,
                                                                                                        time_index,
                                                                                                        sigmas,
                                                                                                        fitting_range,
                                                                                                        0.9)
        special_blob_positions = specialSignalArray()
        for i in range(len(prepare_blob_positions)):
            buffer = np.array([])
            for j in range(len(prepare_blob_positions[i].array)):
                buffer = np.append(buffer, float(time[prepare_blob_positions[i].array[j]]))
            prepare_blob_positions[i].array = buffer
        special_blob_positions.array = prepare_blob_positions
        blob_positions = np.append(blob_positions, special_blob_positions)

        average = np.array([])
        for i in range(len(removed_blobs)):
            average = np.append(average, np.mean(removed_blobs[i].array))
        S_c_avarages = specialSignalArray()
        S_c_avarages.array = np.array([])
        for i in range(len(blobs_array)):
            S_c_avarage = specialSignalArray()
            S_c_avarage.array = np.full(fitting_range * 2, 0)
            for j in range(len(blobs_array[i].array)):
                # plt.plot(np.arange(1,fitting_range*2+1), blobs_array[i].array[j].array)
                S_c_avarage.array = np.sum([S_c_avarage.array, blobs_array[i].array[j].array], axis=0)
            if not len(blobs_array[i].array) == 0:
                S_c_avarage.array /= len(blobs_array[i].array)
            # plt.title("More than one")
            # plt.show()
            # if sigmas[i] == 1.5:
            #     plt.plot(np.arange(1,fitting_range*2+1),  S_c_avarage.array)
            #     plt.title("c_avarage")
            #     plt.show()
            # plt.clf()
            S_c_avarages.array = np.append(S_c_avarages.array, S_c_avarage)
        c_avarage_full = np.append(c_avarage_full, S_c_avarages)

    #Return with the blob positions in time(the structure is on-times->sigmas->blob posizions)
    return blob_positions, c_avarage_full, fitting_range

#=================================================================================
# TEST

def Main():
    T = 0.01
    t_res = 1e-6
    frequencies = [1e4,35e3]
    amplitudes = [1,0]

    (periodic_sig,t1)  = sim.simulate_periodic(frequencies,amplitudes,T=T,t_res=t_res,noise=0.10)
    (gauss_sig, t2)    = sim.simulate_gaussian(N=10000,T=T,t_res=t_res,wt=1e-5,noise=0.10)
    x = detrend(periodic_sig + gauss_sig)


    # Filter test
    fs = 1/t_res
    lowcut = 20e3
    highcut = 50e3
    y = iir_bandpass_filter(x,lowcut,highcut,fs,order=5)
    freq, apsd_y = signal.welch(y, fs, nperseg = 1024)
    freq, apsd_x = signal.welch(x, fs, nperseg = 1024)
    #(apsd_y,phase_x,freq) = cross_spectrum(y,y,fs=fs)
    #(apsd_x,phase_x,freq) = cross_spectrum(x,x,fs=fs)

    plt.loglog(freq, apsd_y)
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.show()

    #fig   = plt.figure()
    #fig.add_subplot(221)
    #plt.plot(x)
    #fig.add_subplot(223)
    #plt.plot(y)
    #fig.add_subplot(224)
    #plt.loglog(freq*1e-3,apsd_y)
    #fig.add_subplot(222)
    #plt.loglog(freq*1e-3,apsd_x)

    #Correlation func. test
    (corr_fft,lag) = ccf(x, x, axis=None) #calculate with FFT

    (corr_def,lag) =ccf_def(x,x) #calculated by definition

    fig   = plt.figure()

    fig.add_subplot(311)
    plt.plot(lag,corr_fft,'r-')
    plt.xlim((-250,250))
    plt.ylim((-0.2,1.2))
    plt.title("CCF calculated via FFT")


    fig.add_subplot(312)
    plt.plot(lag,corr_def)
    plt.xlim((-250,250))
    plt.title("CCF calculated via definition")

    fig.add_subplot(313)
    plt.plot(x)
    plt.show()

if __name__=="__main__":
    Main()
