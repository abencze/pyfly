# -*- coding: utf-8 -*-
'''
=============================================================================
* -------------------
* - Module history: -
* -------------------
*    * Created by A. Bencze in March 2014.
*    * New function added (multiplot_spectra) in December 2014.
	*    * Rename multiplot --> multiplot_rawsignal in December 2014.
	* -------------------
	* - FUNCTIONS: -
	* -------------------
	*    + plot_rawsignal
	*    + plot_full_rawsignal(RR)
	*    + multiplot_rawsignal
	*    + multiplot_spectra
	*    + plot_pdf
	*    + plot_spectrogram
	*    + plot_deflection_period
	*    + plot_rel_fluc_ampl
	*    + plot_correlation
	*    + plot_fluxsurfaces
	*    + plot_noise_vs_signal
	*    + plot_blob_removing(RR)
	*    + plot_blob_distributions(RR)
	*    + sigma_blob_number_ratio_plot(RR)
	*    + plot_blob_velocities(RR)
	*---------------
=============================================================================
	'''
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.colors as colors
import prepare_data as pd
import signal_processing as sp
from scipy import signal as sg
from signal_processing import smooth, fir_bandpass_filter, apsd, rescaled_pdf, \
    signal_splitter, rel_fluc_ampl, ccf
from scipy.optimize import curve_fit


# ==============================================================================
def plot_rawsignal(shot, source='compass-APD', channel=10, t1=0.8, t2=1.8, \
                   offset=True, decim=True, fs=2e6, t0=0.8, digi_res=12, \
                   plot_dot=False):
    '''
    This function plots a given raw signal.
    '''
    raw_signal = pd.get_rawsignal(shot, source=source, channel=channel, t1=t1, t2=t2, \
                                  offset=offset, fs=fs, digi_res=digi_res, t0=t0)
    time = np.array([t * 1 / fs for t in range(raw_signal.size)]) + t1
    if source == 'compass-beam':
        apd_hv = pd.get_apd_hv(shot)
    if decim == True:
        raw_signal = sg.decimate(raw_signal, 50)
        dt = 1.0 / (raw_signal.size - 1)
        time = np.array([t * dt for t in range(np.size(raw_signal))]) + t1

    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    if plot_dot:
        ax1.plot(time, raw_signal, 'o-')

    ax1.plot(time, raw_signal)
    if source == 'compass-beam':
        ax1.set_title(source + ": Shot: #" + str(shot) + ", ADC Channel: " + str(channel) + \
                      ", HV: " + str(apd_hv) + " V")
    else:
        ax1.set_title(source + ": Shot: #" + str(shot) + ", ADC Channel: " + str(channel))
    ax1.set_xlabel("Time [s]", size=12)
    ax1.set_ylabel("signal aplitude [V]")
    plt.show()

# ===============================================================================

def plot_full_rawsignal(shot, source='compass-APD', t1=0.8, t2=1.8, \
                        offset=True, decim=True, fs=2e6, t0=0.8, digi_res=12, \
                        plot_dot=False):
    fig = plt.figure()
    fig.suptitle("Shot #" + str(shot) + " rawsignal")
    for i in range(1, 19):
        data, time, indexes = pd.get_rawsignal(shot, channel=i, source=source, with_time=True,
                                               t1=t1, t2=t2, offset=offset, fs=fs, digi_res=digi_res,
                                               t0=t0, polarity=polarity)
        if decim == True:
            data = sg.decimate(data, 50)
            dt = 1.0 / (data.size - 1)
            time = np.array([t * dt for t in range(np.size(data))]) + t1
        ax = fig.add_subplot(6, 3, i)
        ax.plot(time, data)
        ax.set_title("Channel #" + str(i))
        if i > 15:
            ax.set_xlabel("Time [s]")
        print "Channel #" + str(i) + " readed."
    plt.show()


# ===============================================================================
def multiplot_rawsignal(shot, source='compass-beam',channels=np.arange(1, 19), \
                        t0=0.8, fs=2e6, digi_res=12):
    '''
    This function plots the raw signal of multiple channels.
    '''

    fig = plt.figure()
    for i, channel in enumerate(channels):
        print "Channel: ", channel
        data = pd.get_rawsignal(shot, source=source, polarity=polarity,channel=channel, offset=True, \
                                t1=t0, t2=t0 + 1, digi_res=digi_res)
        time = np.array([t * (1 / fs) for t in range(data.size)]) + t0
        ax = fig.add_subplot(6, 3, i + 1)
        ax.plot(time, data, label='ch: ' + str(channel))
        fig.suptitle('Signal Overview for shot #' + str(shot), fontsize=20)
        plt.legend()
    plt.show()


# ===============================================================================
def multiplot_spectra(shot, source='compass-beam', trange_on=[0.912, 1.912], \
                      trange_off=[1.6, 1.8], \
                      channels=np.arange(1, 19), n_row=6, n_column=3, fs=2e6, \
                      digi_res=14, window_len=1, plot_xlim=[], plot_ylim=[]):
    '''
    Plots the power spectrum for each channel determined by channels.
    '''
    fig = plt.figure()
    for i, channel in enumerate(channels):
        data_on = pd.get_rawsignal(shot, source=source, channel=channel, \
                                   offset=True, t1=trange_on[0], \
                                   t2=trange_on[1], digi_res=digi_res)
        data_off = pd.get_rawsignal(shot, source=source, channel=channel, \
                                    offset=True, t1=trange_off[0], \
                                    t2=trange_off[1], digi_res=digi_res)
        (P_xx_on, frequency, apsd_error_on) = apsd(data_on, fs, normalization=True)
        (P_xx_off, frequency, apsd_error_off) = apsd(data_off, fs, normalization=True)
        ax = fig.add_subplot(n_row, n_column, i + 1, xscale='linear', yscale='linear')
        ax.plot(frequency, smooth(P_xx_on, window_len=window_len), 'r-', label='ch: ' + str(channel))
        ax.plot(frequency, smooth(P_xx_off, window_len=window_len), 'k--', label='Bg')
        if plot_xlim:
            ax.set_xlim(plot_xlim)
        if plot_ylim:
            ax.set_ylim(plot_ylim)
        fig.suptitle('Spectral Overview for shot #' + str(shot), fontsize=20)
        plt.legend()
    plt.show()


# ===============================================================================
def plot_spectra(shot, source='compass-beam', t0=0.8, trange=[0.912, 1.912], \
                 channel=12, fs=2e6, digi_res=14, df=200, smooth_win=1, \
                 filt=False, lowcut=1e3, highcut=2e5, filt_order=5, \
                 logspace=False, perror=False, f0=1e3, df0=200, \
                 plot_xlim=[], plot_ylim=[], printinfo=False, \
                 normalization=True):
    '''
    Plots the power spectrum for a specified channel in a given time interval.
    '''
    raw_data = pd.get_rawsignal(shot, source=source, channel=channel, offset=True, \
                                t0=t0, t1=trange[0], t2=trange[1], digi_res=digi_res)

    if filt:
        data, b = fir_bandpass_filter(raw_data, filt_order, lowcut, highcut, fs, window='hamming')
        label_text = 'channel:     ' + str(channel) + '\n' \
                     + 'trange    = [' + str(trange[0]) + ', ' + str(trange[1]) + ']' + '\n' \
                     + 'smooth_win = ' + str(smooth_win) + '\n' \
                     + 'lowcut     = ' + str(lowcut * 1e-3) + ' kHz' + '\n' \
                     + 'highcut    = ' + str(highcut * 1e-3) + ' kHz' + '\n' \
                     + 'filt_order = ' + str(filt_order)
    else:
        data = raw_data
        label_text = 'ch: ' + str(channel) + '\n' \
                     + 'trange = [' + str(trange[0]) + ', ' + str(trange[1]) + ']' + '\n' \
                     + 'smooth_wind = ' + str(smooth_win)

    Pxx, freq, error = apsd(data, fs, df=df, logspace=logspace, f0=f0, df0=df0, \
                            normalization=normalization, printinfo=printinfo)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, xscale='log', yscale='log')
    if perror:
        ax.errorbar(freq, smooth(Pxx, smooth_win), yerr=error, label=label_text)
    else:
        ax.plot(freq, smooth(Pxx, smooth_win), label=label_text)
    ax.set_xlabel(r'Frequency $[\mathrm{Hz}]$')
    if plot_xlim:
        ax.set_xlim(plot_xlim)
    if plot_ylim:
        ax.set_ylim(plot_ylim)
    if normalization:
        ax.set_ylabel(r'PSD [$\mathrm{V^2/Hz}$]')
    else:
        ax.set_ylabel(r'PSD [a.u.]')
    fig.suptitle('Spectral overview for shot #' + str(shot), fontsize=18)
    plt.legend()
    plt.show()


# ===============================================================================
def compare_spectra(source='compass-beam', shot1=7727, shot2=7728, ch1=10, ch2=10, \
                    t_int1=[1.155, 1.194], t_int2=[1.155, 1.194], fs=2e6, \
                    t0=0.8, window_len=1, filt=False, digi_res=12, \
                    lowcut=1e3, df=8e2, normalization=True, logspace=False, \
                    plot_xlim=[], plot_ylim=[],xscale='log',yscale='log'):
    '''
    This function plots spectrum of two signals. This could be
    from two different shots, two differen channels or two different
    time intervals.
    '''

    data_1 = pd.get_rawsignal(shot1, source=source, channel=ch1, offset=True, \
                              t0=t0,t1=t_int1[0], t2=t_int1[1],digi_res=digi_res)
    data_2 = pd.get_rawsignal(shot2, source=source, channel=ch2, offset=True, \
                              t0=t0,t1=t_int2[0], t2=t_int2[1],digi_res=digi_res)

    if filt:
        # ----------------------------------------------------------------------
        # CHEBYSHEV 2 IIR HIGHPASS  FILTER:
        # ----------------------------------------------------------------------

        ws = 2 * lowcut / fs
        wp = 2 * df / fs + ws
        gpass = 25
        gstop = 62

        b, a = sg.iirdesign(wp, ws, gpass, gstop, analog=False, ftype='cheby2',\
		                    output='ba')

        data_1 = sg.lfilter(b, a, data_1)
        data_2 = sg.lfilter(b, a, data_2)

    apsd_abs_1, freq_1, apsd_error_1 = apsd(data_1, fs, df=200, logspace=logspace,\
	                                        df0=200,normalization=normalization)
    apsd_abs_2, freq_2, apsd_error_2 = apsd(data_2, fs, df=200, logspace=logspace,\
	                                        df0=200,normalization=normalization)
    # --------------------------------------------------------------------------
    # PLOT:
    # --------------------------------------------------------------------------
    legend_1 = "# " + str(shot1) + ", ch: " + str(ch1) + ", \
	            t=[" + str(t_int1[0]) + ", " + str(t_int1[1]) + "]"
    legend_2 = "# " + str(shot2) + ", ch: " + str(ch2) + ", \
	            t=[" + str(t_int2[0]) + ", " + str(t_int2[1]) + "]"
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111, xscale=xscale, yscale=yscale)
    ax1.plot(freq_1, smooth(apsd_abs_1, window_len=window_len), 'r',\
	         linewidth=1, label=legend_1)
    ax1.plot(freq_2, smooth(apsd_abs_2, window_len=window_len), 'b',\
	         linewidth=1, label=legend_2)
    if plot_xlim:
        ax1.set_xlim(plot_xlim)
    if plot_ylim:
        ax1.set_ylim(plot_ylim)
    ax1.set_xlabel(r'Frequency [$\mathrm{Hz}$]')
    if normalization:
        ax1.set_ylabel(r'APSD $[\mathrm{V^2}/\mathrm{Hz}]$')
    else:
        ax1.set_ylabel(r'APSD $[\mathrm{a.u.}]$')

    ax1.set_title("Compare spectra")
    # -------------------------------------------------------------------------
    ax1.legend(loc='best')
    plt.show()
# ===============================================================================
def plot_spectrogram(shot,source='compass-beam',polarity=-1,channel=11,t0=0.8,\
                     trange=[0.8,1.8],fs=2e6,\
                     NFFT = 2048,noverlap=1024,plot_frange=[1e3,500e3],digi_res=12,\
                     clim=[-150,-80]):

    data      = pd.get_rawsignal(shot,source=source,polarity=polarity,channel=channel,t1=trange[0],\
                                 t2=trange[1],fs=fs,digi_res=digi_res,t0=t0)
    Sxx, f, t, im = plt.specgram(data, NFFT=NFFT, Fs=fs, noverlap=noverlap,\
                                 detrend='linear',cmap='jet',vmin=clim[0],vmax=clim[1])
    #fig = plt.figure()
    #ax  = fig.add_subplot(111)
    plt.ylim(plot_frange)
    plt.title('spectrogram, # '+str(shot)+'Ch: '+str(channel))
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    clb=plt.colorbar()
    clb.set_label('psd [dB]', labelpad=-40, y=1.05, rotation=0)
    plt.show()

# ===============================================================================
def plot_pdf(shot, source='compass-beam', trange=[0.8, 1.8], \
             channel=12, fs=2e6, digi_res=12,t0=0.8, bins=50):
    '''
    By A. Bencze, 2015
    Plots rescaled amplitude distribution of the signal
    Gaussian distribution also shown
    '''
    fig = plt.figure()

    raw_data = pd.get_rawsignal(shot, source=source, channel=channel, \
                                offset=True,t0=t0, t1=trange[0], t2=trange[1],\
								digi_res=digi_res)
    gauss_data = np.random.randn(1, np.size(raw_data))
    (gpdf, gbins_vec) = rescaled_pdf(gauss_data, bins=bins)
    (pdf, bins_vec) = rescaled_pdf(raw_data, bins=bins)

    label_text = 'ch: ' + str(channel) + '\n' \
                 + 'trange = [' + str(trange[0]) + ', ' + str(trange[1]) + ']' + '\n' \
                 + 'bins = ' + str(bins)
    ax = fig.add_subplot(1, 1, 1, xscale='linear', yscale='log')
    ax.plot(bins_vec, pdf, label=label_text)
    ax.plot(gbins_vec, gpdf, '--r', label='Gauss distribution')
    ax.set_xlabel(r"$\xi=\frac{\left(x-\langle x\rangle\right)}{\langle (x-\langle x\rangle)^2\rangle}$", \
                  size=14)
    ax.set_ylabel(r"$PDF(\xi)$")
    fig.suptitle('Amplitude distribution #' + str(shot), fontsize=20)
    plt.legend()
    plt.show()


# ===============================================================================
def plot_deflection_period(shot, source='compass-beam', fs=2e6, digi_res=12,\
                           channels=[9, 10, 11, 12, 13, 14, 15, 16], t0=0.8,\
                           trange=[0.8, 1.8], shift=0, period=8,polarity=1):
    '''
    By A. Bencze, 2015
    Plots data samples averaged over the deflection periods
    '''
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i, channel in enumerate(channels):
        print 'channel:', channels[i]
        data = pd.get_rawsignal(shot, source=source, channel=channels[i], \
                                offset=True, t1=trange[0], t2=trange[1],t0=t0,\
								digi_res=digi_res, fs=fs,polarity=polarity)
        periods = signal_splitter(data[shift:], period)
        mean_period = np.mean(periods, axis=0)
        ax.plot((mean_period - mean_period[3]) + i * 0.1, 'o--r', markersize=8, \
                markerfacecolor='black')
        plt.text(-2, mean_period[0] - mean_period[3] + 0.1 * i, 'ch:' + str(channel))
    ax.set_xlim(-3, 10)
    ax.set_xlabel('samples')
    ax.get_yaxis().set_ticks([])
    fig.suptitle('Average deflection period for  shot #' + str(shot) + ',\n' \
                 + 'trange = [' + str(trange[0]) + ', ' + str(trange[1]) + ']', \
                 fontsize=14)
    plt.show()
# ===============================================================================
def plot_rel_fluc_ampl(shot, source='compass-beam',polarity=-1,t0=0.8,\
                       channels=[7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17],\
                       trange_on=[1.046, 1.066], trange_off=[1.067, 1.086],\
                       fint_noise=[2e5, 5e5], fs=2e6, f0=1e3, df=200):
    rfa_prof = np.empty(len(channels))
    for i, channel in enumerate(channels):
        print 'Ch: #' + str(channel)
        rfa_prof[i] = rel_fluc_ampl(shot, source=source,polarity=polarity,t0=t0,\
		                            channel=channel, trange_on=trange_on,\
                                    trange_off=trange_off, fint_noise=fint_noise,\
									fs=fs, f0=f0,df=df)
    print rfa_prof
    label = '#' + str(shot) + ',\n' + ' t_on=[' + str(trange_on[0]) + ',' + str(trange_on[1]) + ']'
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(channels, rfa_prof * 100, '--o', linewidth=2, color='black', label=label)
    ax.set_ylim([-5, 100])
    ax.set_xlabel('channel number')
    ax.set_ylabel(r'$\mathrm{Rel. Fluc. Ampl. [\%]}$')
    ax.legend()
    plt.show()


# ===============================================================================
def plot_correlation(shot, source='compass-beam', ref_ch=13, plot_ch=13,\
                     digi_res=12, t0=0.8,trange=[1.1, 1.2], filt=False,\
                     frange=[1e3, 200e3], filter_order=10, fs=2e6,\
					 tau_range=[-100, 100], norm=False):
    if ref_ch == plot_ch:
        ref_data = pd.get_rawsignal(shot, source=source,channel=ref_ch, \
                                    offset=True,digi_res=digi_res, t0=t0,t1=trange[0], t2=trange[1])
        plot_data = ref_data
    else:
        ref_data = pd.get_rawsignal(shot, source=source,channel=ref_ch, \
                                    offset=True, digi_res=digi_res,t0=t0,t1=trange[0], t2=trange[1])
        plot_data = pd.get_rawsignal(shot, source=source,channel=plot_ch, \
                                     offset=True,digi_res=digi_res,t0=t0,t1=trange[0], t2=trange[1])

    if filt:
        lowcut = frange[0]
        highcut = frange[1]
        filt_order = filter_order
        data_1, b = fir_bandpass_filter(ref_data, filt_order, lowcut, highcut, fs, window='hamming')
        data_2, b = fir_bandpass_filter(plot_data, filt_order, lowcut, highcut, fs, window='hamming')
    else:
        data_1 = ref_data
        data_2 = plot_data
#===============================================================================
    #TEST:
    #fig_test=plt.figure()
    #ax_test = fig_test.add_subplot(211)
    #ax_test.plot(ref_data)
    #ax_test2 = fig_test.add_subplot(212)
    #ax_test2.plot(sg.detrend(data_2,type='linear'))
# ===============================================================================

    corr, lag = ccf(sg.detrend(data_1,type='linear'),sg.detrend(data_2,type='linear'), norm=norm)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(lag * (1 / fs) * 1e6, corr)
    ax.set_xlim(tau_range)
    ax.set_title('# '+str(shot)+', ref_ch: '+str(ref_ch)+', plot_ch: '+str(plot_ch))
    ax.set_xlabel(r'$\tau [\mu s] $')
    ax.set_ylabel('Correlation [a.u.]')
    plt.show()


# ===============================================================================
def plot_fluxsurfaces(shot, time, color_bar=False, plot_separatrix=True, levels=7):
    '''
	Plot the flux surfaces of the shot at the time

	Parameters:
    		@shot: shot number
    		@time: time in [s]
		@color_bar: if it's true, plot color bar(and fill the contour)
		@plot_separatrix: it it's true the separatris plots
		@levels: number of contour levels
    '''
    from pyCDB import client
    cdb = client.CDBClient()
    signal = cdb.get_signal("psi_RZ/EFIT:" + str(shot))

    x_axis = np.linspace(0.3, 0.8, 33)
    y_axis = np.linspace(-0.4, 0.4, 33)

    time_number = (float(time) * 1000) - 970

    plt.clf()

    if plot_separatrix:
        separatrix = cdb.get_signal("RZ_bound/EFIT:" + str(shot))
        separatrix_plot = plt.plot(separatrix.data[time_number]['R'], separatrix.data[time_number]['Z'], color='red',
                                   lw=3, label="Separatrix")
        plt.legend()

    if not color_bar:
        contour = plt.contour(x_axis, y_axis, np.transpose(signal.data[time_number]), levels, origin='lower',
                              cmap=plt.cm.bone)
    else:
        contour = plt.contourf(x_axis, y_axis, np.transpose(signal.data[time_number]), levels, origin='lower',
                               cmap=plt.cm.bone)
        cbar = plt.colorbar(contour)
        cbar.ax.set_ylabel('Magnetic field [Wb/rad]')

    plt.title("Flux surfaces calculated by EFIT for shot #" + str(shot) + " at " + str(time) + " ms")
    plt.xlabel("Radial position [m]")
    plt.ylabel("Vertical position [m]")
    plt.show(block=False)


# ===============================================================================
def plot_separatrix_position(shot, source='compass-beam'):
    from pyCDB import client
    cdb = client.CDBClient()
    separatrix = cdb.get_signal("RZ_bound/EFIT:" + str(shot))
    positions = np.array([0])
    time = np.array([0])
    size = np.shape(separatrix.data[1]['R'])
    for time_index in range(302):
        zeros = []
        for index in range(361):
            if index != 0:
                if (separatrix.data[time_index]['Z'][index] > 0 and separatrix.data[time_index]['Z'][
                        index - 1] < 0) or (
                        separatrix.data[time_index]['Z'][index] < 0 and separatrix.data[time_index]['Z'][
                        index - 1] > 0):
                    if np.abs(separatrix.data[time_index]['Z'][index]) > (separatrix.data[time_index]['Z'][index - 1]):
                        zeros = np.append(zeros, index - 1)
                    else:
                        zeros = np.append(zeros, index)
        if np.size(zeros) < 2:
            print separatrix.data[time_index]['Z']
        if separatrix.data[time_index]['R'][zeros[0]] > separatrix.data[time_index]['R'][zeros[1]]:
            if positions[0] == 0:
                positions = np.array([separatrix.data[time_index]['R'][zeros[0]]])
            else:
                positions = np.append(positions, separatrix.data[time_index]['R'][zeros[0]])
        else:
            if positions[0] == 0:
                positions = np.array([separatrix.data[time_index]['R'][zeros[1]]])
            else:
                positions = np.append(positions, separatrix.data[time_index]['R'][zeros[1]])
        if time[0] == 0:
            time = np.array([(float(time_index) / 1000) + 0.970])
        else:
            time = np.append(time, (float(time_index) / 1000) + 0.970)
    time[0] = 0.97
    plt.plot(time, positions)
    plt.title('#' + str(shot) + " shot's separatrix radial position")
    plt.xlabel('Time[s]')
    plt.ylabel('Radial position[m]')
    plt.show(block=False)


# ===============================================================================
def plot_noise_vs_signal(shot, source='compass-beam', channel=10, trange=[0.8, 1.8], \
                         t0=0.8, fs=2e6, digi_res=14, n_points=2000, filt_range=[2e5, 4e5], \
                         filt_order=100, bandwidth=1e6, plot_xlim=[], plot_ylim=[]):
    '''
    This function plots the signal-to-noise ratio based on the model of the noise consisting
    of a sum of the photon noise and the aplifier electric noise.
    INPUT: same as for the get_rawsignal
           n_points: number of samples for signal level calculation (subinterval length)
           filt_range: frequency range for noise calculation
           filt_order: the filter order used for the FIR filter used for filtering data
           bandwidth: the effective bandwidth of the measurement. This parameter can be adjusted
                      until the fitted noise at signal=0 matches the measured noise rms.
    '''
    data = pd.get_rawsignal(shot, source=source,channel=channel, \
                            offset=True, t1=trange[0], t2=trange[1], \
                            digi_res=digi_res, t0=t0)
    #TEST:
    #plt.figure()
    #plt.plot(data)

    data_filt, b = fir_bandpass_filter(data, filt_order, \
                                       filt_range[0], filt_range[1], fs, \
                                       window='hamming')
    bg = data[0:2000]
    dd = signal_splitter(data, n_points)
    nn = signal_splitter(data_filt, n_points)
    n_int = np.shape(dd)[0]

    n = np.zeros(n_int)
    s = np.zeros(n_int)
    for n_slice in range(n_int):
        x = dd[n_slice, :]
        x_filt = nn[n_slice]
        n_int = (1.0 / len(x_filt)) * np.sum(x_filt ** 2)
        n[n_slice] = n_int * bandwidth / (filt_range[1] - filt_range[0])  # noise power
        s[n_slice] = np.mean(x)  # signal level

    p = np.polyfit(s, n, 1)
    xx = np.linspace(0, np.max(s) * 1.05, 100)
    yy = p[0] * xx + p[1]
    print '##########################################'
    print 'linear fit: y=' + str(p[0]) + '*x+' + str(p[1])
    print '##########################################'
    print '+ noise level at 0 signal:       %.2f mV' % (np.sqrt(yy)[0] * 1000)
    print '+ background standard deviation: %.2f mV' % (np.std(bg) * 1000)
    snr = xx / np.sqrt(yy)
    # ---------------------------------------------------------------------------
    fig = plt.figure()
    ax = fig.add_subplot(121)
    ax.plot(s, n, 'o')
    ax.yaxis.major.formatter.set_powerlimits((0, 0))
    fig_text = 'offset STD: %.2f mV' % (np.std(bg) * 1000) + ', noise lev.(s=0): %.2f mV' % (np.sqrt(yy)[0] * 1000)
    ax.text(0.01, yy[0] / 2, fig_text)
    ax.plot(xx, yy, 'r-')
    if plot_xlim == []:
        ax.set_xlim([0, np.max(s) * 1.05])
    else:
        ax.set_xlim(plot_xlim)
    if plot_ylim == []:
        ax.set_ylim([0, np.max(n) * 1.05])
    else:
        ax.set_ylim(plot_ylim)
    ax.set_title('#' + str(shot) + '/CH:' + str(channel))
    ax.set_xlabel('signal level [V]')
    ax.set_ylabel('noise power [V^2]')
    ax = fig.add_subplot(122)
    ax.plot(xx, snr, 'b-')
    ax.set_xlabel('signal level [V]')
    ax.set_ylabel('SNR')
    plt.show()

# ===============================================================================

def plot_blob_removing(plottable_information, shot, sigmas, time):
    f, axarr = plt.subplots(len(sigmas), sharex=True, sharey=True)
    plt.suptitle("#" + str(shot) + u" Detektált blobok")
    plt.xlabel("t [s]")
    mean = np.mean(plottable_information[0].data)
    sigma = np.std(plottable_information[0].data)
    for i in range(len(sigmas)):
        axarr[i].set_ylabel("Jel [V]")
        axarr[i].grid()
        if i < len(sigmas):
            axarr[i].set_title("Limit: " + str(sigmas[i]) + "$\sigma$", fontsize=10)
            if np.size(plottable_information[i].blobs) == 0:
                signal_plot, sigma_plot, mean_plot = axarr[i].plot(time, plottable_information[i].data, 'k', time,
                                                                   np.full(np.size(time), mean + sigma * sigmas[i]),
                                                                   'r', time, np.full(np.size(time), mean), 'b')
            else:
                signal_plot, blobs_plot, sigma_plot, mean_plot = axarr[i].plot(time, plottable_information[i].data, 'k',
                                                                               time[plottable_information[i].blobs],
                                                                               plottable_information[i].data[
                                                                                   plottable_information[i].blobs],
                                                                               'ro', time,
                                                                               np.full(np.size(time),
                                                                                       mean + sigma * sigmas[i]), 'r',
                                                                               time,
                                                                               np.full(np.size(time), mean), 'b')
            plt.setp(mean_plot, linewidth=3)
    plt.show()

# ==================================================================================================================
def exponenial_func(x, a, b, c):
	return a*np.exp(-b*x)+c
def plot_blob_distributions(blob_positions, channel, shot, sigmas, times):
    fig, ax1 = plt.subplots()
    time_delays = np.array([])
    for i in reversed(range(len(sigmas))):
        time_el = 0
        size = 0
        for j in range(len(times[0])):
            local_time_el = 0
            if len(blob_positions[j].array[i].array) - 1 > 0:
                size += 1
                for k in range(len(blob_positions[j].array[i].array) - 1):
                    local_time_el += blob_positions[j].array[i].array[k + 1] - blob_positions[j].array[i].array[k]
                local_time_el /= len(blob_positions[j].array[i].array) - 1
                time_el += local_time_el
        time_el /= size
        time_delays = np.append(time_delays, time_el)
    mpl.rc('font', **{'sans-serif': 'Arial',
                      'family': 'sans-serif'})
#===============================================================================
    popt, pcov = curve_fit(exponenial_func, sigmas, time_delays)
    xx = np.linspace(0.5, 4, 100)
    yy = exponenial_func(xx, *popt)
#===============================================================================
    ax1.plot(sigmas, time_delays, "bo",xx,yy,'--b')
    ax1.set_title("#" + str(shot) + ", " + "CH: "+str(channel))
    ax1.set_xlabel(u"$\sigma$")
    ax1.set_ylabel(u"Waiting times [s]", color="b")
    plt.grid()
    ax2 = ax1.twinx()
    number_of_blobs = np.array([])
    for i in reversed(range(len(sigmas))):
        time_el = 0
        size = 0
        for j in range(len(times[0])):
            if len(blob_positions[j].array[i].array) > 0:
                size += 1
                time_el += len(blob_positions[j].array[i].array)

        time_el /= size
        number_of_blobs = np.append(number_of_blobs, time_el)
    mpl.rc('font', **{'sans-serif': 'Arial',
                      'family': 'sans-serif'})
#===============================================================================
    popt_nblob, pcov = curve_fit(exponenial_func, sigmas, number_of_blobs)
    xx_nblobs = np.linspace(0.5, 4, 100)
    yy_nblobs = exponenial_func(xx_nblobs, *popt_nblob)
#===============================================================================
    ax2.plot(sigmas, number_of_blobs, "ro",xx_nblobs,yy_nblobs,"--r")
    ax2.set_ylabel(u"No. of detected blobs", color="r")
    plt.show()


def automatic_plot_blob_distributions(shot=11348, source="compass-local", timerange=[0.9, 1.9], channel=15, sigmas=[],
                                      fitting_range_msec=0, filter_low = 20, filter_high = 100, filt_order = 5):
    times = sp.select_time(shot=shot, channel=12, source=source, timerange=timerange, GUI=False)
    blob_positions, trash1, trash2 = sp.count_blob_positions(shot=shot, source=source, channel=channel, times=times, sigmas=sigmas,
                                             fitting_range_msec=fitting_range_msec, filter_low = filter_low, filter_high = filter_high, filt_order = filt_order)
    plot_blob_distributions(blob_positions=blob_positions, channel=channel, shot=shot, sigmas=sigmas, times=times)


# ===============================================================================================================

def plot_blob_time_delay_histogram(blob_positions, channel, shot, sigmas, times, min_simga=0, max_sigma=np.inf,t_bins=10):
    time_delays = np.array([])
    for i in reversed(range(len(sigmas))):
        if sigmas[i] < min_simga or sigmas[i] > max_sigma:
            continue
        for j in range(len(times[0])):
            if len(blob_positions[j].array[i].array) - 1 > 0:
                for k in range(len(blob_positions[j].array[i].array) - 1):
                    time_delays= np.append(time_delays, blob_positions[j].array[i].array[k + 1] - blob_positions[j].array[i].array[k])
    mpl.rc('font', **{'sans-serif': 'Arial',
                      'family': 'sans-serif'})
    (t_hist,t_w) = np.histogram(time_delays*1e3,bins=t_bins)
#===============================================================================
    popt_waittime, pcov = curve_fit(exponenial_func, t_w[1:], t_hist)
    xx_waittime = np.linspace(0, 6, 100)
    yy_waittime = exponenial_func(xx_waittime, *popt_waittime)
#===============================================================================
    #plt.hist(time_delays*1e3,bins=t_bins)
    plt.plot(xx_waittime,yy_waittime,'--r',linewidth=2)
    plt.plot(t_w[1:],t_hist,'ob',markersize=10)
    min_simga_text = str(max(min_simga, min(sigmas)))
    max_sigma_text = str(min(max_sigma, max(sigmas)))
    plt.title("#" + str(shot) + " channel: " + str(channel) + u", waiting times in range: [" + min_simga_text + "-" + max_sigma_text +"]"+ r"$\times\sigma$")
    plt.xlabel(r"$\tau_w$ [ms]")
    plt.xlabel(r"$\tau_w$ [ms]")
    plt.ylabel(r"PDF ($\tau_w$)")
    plt.show()

def automatic_plot_blob_time_delay_histogram(shot=11348, source="compass-local", timerange=[0.9, 1.9], channel=15, sigmas=[],
                                      fitting_range_msec=0, filter_low = 20, filter_high = 100, filt_order = 5, min_sigma = 0, max_sigma = np.inf,t_bins=10):
    times = sp.select_time(shot=shot, channel=12, source=source, timerange=timerange, GUI=False)
    blob_positions, trash1, trash2 = sp.count_blob_positions(shot=shot, source=source, channel=channel, times=times, sigmas=sigmas,
                                             fitting_range_msec=fitting_range_msec, filter_low=filter_low,
                                             filter_high=filter_high, filt_order=filt_order)
    plot_blob_time_delay_histogram(blob_positions, channel, shot, sigmas, times, min_simga=min_sigma, max_sigma=max_sigma,t_bins=t_bins)

# =====================================================================================

def sigma_blob_number_ratio_plot(info, sigmas):
    blob_numbers = []
    for i in info:
        if len(blob_numbers) == 0:
            blob_numbers = [np.size(i.blobs)]
        elif i != info[-1]:
            blob_numbers = np.append([blob_numbers], [np.size(i.blobs)])
    plt.clf()
    plt.plot(sigmas, blob_numbers)
    plt.title("Blobs number - $\sigma$-s ratio")
    plt.xlabel("$\sigma$")
    plt.ylabel("Number of blobs")
    plt.yscale('log')
    plt.show()

# ================================================================================================================

    '''
    This function calculate and plot the blob velocities between channel 15 and 16.
    In the plot, the red signal is from channel 15, the blue signal from channel 16, the black gauss is a
    fit on channel 16, to help find the maximum.

    Parameters:
        shot: shot number
        timerange: the range of time(tip: best timerange where the separatrix position stationary)
        times: the selected on and off times at the timerange (use sp.select_time())
        fitting_range: the gauss fittin gange in microsecs
        sigmas: the array of sigmas
        gauss_fit_lower/upper_bound: the bound of gauss fitting
    '''


def plot_blob_velocities(shot, source, timerange, times, fitting_range, sigmas, gauss_fit_lower_bound=[0, -10, 0],
                         gauss_fit_upper_bound=[100, 10, 100], filter_low = 20, filter_high = 100, filt_order = 5, channel1 = 15, channel2 = 16):
    blob_positions, trahs1, trash2  = sp.count_blob_positions(shot=shot, source=source, channel=15, times=times, sigmas=sigmas,
                                             fitting_range_msec=fitting_range, filter_low = filter_low, filter_high = filter_high, filt_order = filt_order)
    sigmas = np.sort(sigmas)[::-1]

    data, time, index = pd.get_rawsignal(shot, source=source, channel=channel1, t1=timerange[0],
                                         t2=timerange[1], with_time=True)
    data2, time2, index2 = pd.get_rawsignal(shot, source=source, channel=channel2, t1=timerange[0],
                                            t2=timerange[1], with_time=True)

    mpl.rc('font', **{'sans-serif': 'Arial',
                      'family': 'sans-serif'})
    fig, ax = plt.subplots(len(blob_positions[0].array), sharex=True, sharey=True)
    for s in range(len(blob_positions[0].array)):
        blob_s_1 = []
        blob_s_2 = []
        for i in range(len(blob_positions)):
            for k in range(len(blob_positions[i].array[s].array)):
                index = np.where(time == blob_positions[i].array[s].array[k])
                indexes = np.arange(index[0] - fitting_range, index[0] + fitting_range, 1)
                # normalization
                data /= np.max(data[indexes])
                data2 /= np.max(data2[indexes])
                if blob_s_1 == []:
                    blob_s_1 = np.array(data[indexes])
                    blob_s_2 = np.array(data2[indexes])
                else:
                    blob_s_1 = np.sum([blob_s_1, data[indexes]], axis=0)
                    blob_s_2 = np.sum([blob_s_2, data2[indexes]], axis=0)
        if (len(blob_s_1) != 0) or (len(blob_s_2) != 0):
            blob_s_1 = blob_s_1 / np.max(blob_s_1)
            blob_s_2 = blob_s_2 / np.max(blob_s_2)
            new_indexes = np.where(np.logical_and(blob_s_1 != np.max(blob_s_1), blob_s_2 != np.max(blob_s_2)))
            blob_s_1 = blob_s_1[new_indexes] / np.max(blob_s_1[new_indexes])
            blob_s_2 = blob_s_2[new_indexes] / np.max(blob_s_2[new_indexes])
            blob_s_1 -= np.full(np.shape(blob_s_1), np.min(blob_s_1))
            blob_s_2 -= np.full(np.shape(blob_s_2), np.min(blob_s_2))
            blob_s_1 = blob_s_1 / np.max(blob_s_1)
            blob_s_2 = blob_s_2 / np.max(blob_s_2)
            indexes = np.arange(- fitting_range, fitting_range, 1)
            indexes = indexes[new_indexes]

            from scipy import exp
            def gaus(x, a, x0, sigma):
                return a * exp(-(x - x0) ** 2 / (2 * sigma ** 2))

            gaussPopt, gaussPcov = curve_fit(gaus, indexes, blob_s_2, bounds=[gauss_fit_lower_bound, gauss_fit_upper_bound])
            xexpB = np.linspace(indexes[0], indexes[-1], 201)
            yexpB = gaussPopt[0] * exp(-(xexpB - gaussPopt[1]) ** 2 / (2 * gaussPopt[2] ** 2))

            ax[s].plot(indexes, blob_s_1, "r", indexes, blob_s_2, "b")
            ax[s].plot(xexpB, yexpB, "k")

            print "Blobok átlagsebessége " + str(sigmas[s]) + " sigmánál" + ":\t " + str(
                1 / (gaussPopt[1])) + " + / - " + str(
                (1 / (gaussPopt[1])) * np.diag(gaussPcov)[1] / gaussPopt[1]) + " cm/microsec"

            ax[s].grid()
            ax[s].set_ylim(0, 1.1)
            if s == 0:
                ax[s].set_title("#" + str(shot) + '\n' + str(sigmas[s]) + r'$\sigma$' + u"-nál nagyobb")
            else:
                ax[s].set_title(str(sigmas[s]) + "-" + str(sigmas[s - 1]) + r'$\sigma$' + u" között")
            if s == len(blob_positions[0].array) - 1:
                ax[s].set_xlabel(u"Centrált időskála [$\mu s$]")
    plt.show()


def automatic_plot_blob_velocities(shot, source, timerange, fitting_range, sigmas, gauss_fit_lower_bound=[0, -10, 0],
                                   gauss_fit_upper_bound=[100, 10, 100], channel1 = 15, channel2 = 16):
    times = sp.select_time(shot=shot, channel=12, source=source, timerange=timerange, GUI=False)
    plot_blob_velocities(shot, source, timerange, times, fitting_range, sigmas, gauss_fit_lower_bound=gauss_fit_lower_bound,
                         gauss_fit_upper_bound=gauss_fit_upper_bound, channel1= channel1, channel2=channel2)

def automatic_plot_blob_velocities_for_inter_ELM_ranges(shot, source, timerange, fitting_range, sigmas, gauss_fit_lower_bound=[0, -10, 0],
                                   gauss_fit_upper_bound=[100, 10, 100], channel1 = 15, channel2 = 16):
    data, time, index = pd.get_rawsignal(shot=shot, source=source, channel=12, t1=timerange[0], t2=timerange[1],
                                         with_time=True)

    #Fit for horizontal data
    def sec(x, a, b, c):
        return a * x * x + b * x + c

    fit, err = curve_fit(sec, time, data)

    #data modifiing to cut ELM phases
    data2 = fit[0] * data * data + fit[1] * data + fit[2]
    mean = np.mean(data2)
    for i in range(len(data2)):
        if data2[i] < mean:
            data2[i] = mean
    data2 *= -1
    mean = (np.max(data2) + np.min(data2)) / 2
    for i in range(len(data2)):
        if data2[i] < mean:
            data2[i] = mean

    times = sp.selecting_times(data2, time, min_interval=0.1, edges=[])

    plot_blob_velocities(shot=shot, source=source, timerange=timerange, times=times, fitting_range=fitting_range,
                            sigmas=sigmas, channel1=channel1, channel2=channel2, gauss_fit_lower_bound=gauss_fit_lower_bound, gauss_fit_upper_bound=gauss_fit_upper_bound)

# =====================================================================================

def automatic_plot_blob_shape(shot=11348, source="compass-local", timerange=[0.9, 1.9], channel=15, sigmas=[],
                              fitting_range_msec=0,
                              filter_low=20, filter_high=100, filt_order=5, bounds_left_low=[0, -1, -1],
                              bounds_left_up=[1, 1, 1], bounds_right_low=[0, -1, -1], bounds_right_up=[1, 1, 1]):
    times = sp.select_time(shot=shot, channel=12, source=source, timerange=timerange, GUI=False)
    plot_blob_shape(shot=shot, source=source, channel=channel, times=times, sigmas=sigmas,
                    fitting_range_msec=fitting_range_msec,
                    filter_low=filter_low, filter_high=filter_high, filt_order=filt_order,
                    bounds_left_low=bounds_left_low, bounds_left_up=bounds_left_up, bounds_right_low=bounds_right_low,
                    bounds_right_up=bounds_right_up)


def plot_blob_shape(shot=11348, source="compass-local", channel=15, times=[], sigmas=[], fitting_range_msec=0,
                    filter_low=20, filter_high=100, filt_order=5, bounds_left_low=[0, -1, -1], bounds_left_up=[1, 1, 1],
                    bounds_right_low=[0, -1, -1], bounds_right_up=[1, 1, 1]):
    blob_positions, c_average, fitting_range = sp.count_blob_positions(shot=shot, source=source, channel=channel,
                                                                       times=times, sigmas=sigmas,
                                                                       fitting_range_msec=fitting_range_msec,
                                                                       filter_low=filter_low, filter_high=filter_high,
                                                                       filt_order=filt_order)
    plot_conditional_av(c_average, fitting_range, sigmas, times, bounds_left_low, bounds_left_up, bounds_right_low,
                        bounds_right_up)


def plot_conditional_av(c_avarage_full, fitting_range, sigmas, times, bounds_left_low, bounds_left_up, bounds_right_low,
                        bounds_right_up):
    c_avarages_in_times = c_avarage_full[0].array
    for j in range(len(sigmas)):
        for i in range(1, len(times[0])):
            c_avarages_in_times[0].array = np.sum((c_avarages_in_times[0].array, c_avarage_full[i].array[j].array),
                                                  axis=0)
        c_avarages_in_times[0].array /= len(times[0])
    xarray = np.arange(-fitting_range, fitting_range)

    def exp(x, a, b, c):
        return a * np.exp(-b * (x)) + c

    fig, ax = plt.subplots(len(sigmas), sharey=True, sharex=True)
    for i in range(len(sigmas)):
        index1 = np.where(np.logical_and(xarray > int(-fitting_range / 2), xarray < 0))
        index2 = np.where(np.logical_and(xarray > 0, xarray < int(fitting_range / 2)))
        index = np.concatenate((index1, index2), axis=1)
        #belowePopt, belovePcov = curve_fit(exp, xarray[index1], c_avarages_in_times[i].array[index1])
        #abovePopt, abovePcov = curve_fit(exp, xarray[index2], c_avarages_in_times[i].array[index2])
        belowePopt, belovePcov = curve_fit(exp, xarray[index1], c_avarages_in_times[i].array[index1],
                                           bounds=(bounds_left_low, bounds_left_up))
        abovePopt, abovePcov = curve_fit(exp, xarray[index2], c_avarages_in_times[i].array[index2],
                                         bounds=(bounds_right_low, bounds_right_up))
        xexpB = np.linspace(xarray[0], 0, 100)
        yexpB = belowePopt[0] * np.exp(-belowePopt[1] * (xexpB)) + belowePopt[2]
        xexpA = np.linspace(0, xarray[-1], 100)
        yexpA = abovePopt[0] * np.exp(-abovePopt[1] * (xexpA)) + abovePopt[2]
        ax[i].plot(xarray[index], c_avarages_in_times[i].array[index], "kx",markersize=8)
        ax[i].plot(xexpB, yexpB, "b", linewidth=2, label=r'$\tau$ = ' + str(1 / -belowePopt[1]) + u" $\mu s$")
        ax[i].plot(xexpA, yexpA, "r", linewidth=2, label=r'$\tau$ = ' + str(1 / -abovePopt[1]) + u" $\mu s$")
        plt.yticks([])
        if i == 0:
            plt.figure()

            plt.plot(xarray[index], c_avarages_in_times[i].array[index], "kx",markersize=8)
            plt.plot(xexpB, yexpB, "b", linewidth=2, label=r'$\tau$ = ' + str(1 / -belowePopt[1]) + u" $\mu s$")
            plt.plot(xexpA, yexpA, "r", linewidth=2, label=r'$\tau$ = ' + str(1 / -abovePopt[1]) + u" $\mu s$")
            plt.yticks([])
            plt.title("> "+str(sigmas[i]) + r'$\sigma$')
            plt.xlabel(u'relative time [$\mu s$]')
            plt.ylabel(u'blob amplitude')
            plt.legend(prop={'size': 10})
        if i == 0:
            ax[i].set_title("> "+str(sigmas[i]) + r'$\sigma$')
        else:
            ax[i].set_title(str(sigmas[i]) + "-" + str(sigmas[i - 1]) + r'$\sigma$')

        ax[i].legend(prop={'size': 10})
        mpl.rc('font', **{'sans-serif': 'Arial',
                          'family': 'sans-serif'})
        ax[i].set_ylabel(u"blob amplitude")
        if i == len(sigmas) - 1:
            ax[i].set_xlabel(u"relative time"+u"[$\mu s$]")
        ax[i].grid()
    fig.subplots_adjust(top=0.96, bottom=0.05, right=0.96)
    plt.show()
